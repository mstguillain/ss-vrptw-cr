/*
Copyright 2018 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/




#include "VRP_instance.h"
#include "VRP_solution.h"
#include "VRP_scenario.h"
#include "NM.h"
#include "LS_Program.h"
#include "tools.h"
#include <queue>
#include <cmath>
#include <getopt.h>
#include <chrono>
#include <ctime>
#include <iostream>
#include <string>


#include "json.hpp"
using json = nlohmann::json;

using namespace std;

bool request_lt(const VRP_request& r1, const VRP_request& r2) {
	if (r1 == r2) return false;

	if (r1.revealTime < r2.revealTime)							// reveal time first
		return true;
	if (r1.revealTime == r2.revealTime) {
		if (r1.l < r2.l)										// end of time window second)
			return true;
		if (r1.l == r2.l) {
			ASSERT(r1.getVertex().getRegion() != r2.getVertex().getRegion(), "no tie breaking !");
			return r1.getVertex().getRegion() < r2.getVertex().getRegion();		// region number to break ties
		}
	}

	return false;
}



string getFileName(const string& s) {
	char sep = '/';
	size_t i = s.rfind(sep, s.length());
	if (i != string::npos)
		return(s.substr(i+1, s.length() - i));
	return("");
}

int main(int argc, char **argv) {
	if (argc < 2) {
		cout << "Usage: eval filename" << endl;
		return 1;
	}
	const char* filename = argv[1];
	std::ifstream jfile;
	jfile.open(filename);
	json j; jfile >> j; jfile.close();


	VRP_request::request_comparison_lt = &request_lt;

	string instance_path 	= "/Users/stguillain/Dropbox/UCL/PhD/VRP/benchmarks/police";
	string graph_file 		= instance_path + "/" + getFileName(j["Instance_Files_Graph"]);
	string carrier_file 	= instance_path + "/" + getFileName(j["Instance_Files_Carrier"]);
	string customers_file 	= instance_path + "/" + getFileName(j["Instance_Files_Customers"]);

	cout << graph_file << endl;
	cout << carrier_file << endl;
	cout << customers_file << endl;

	int vehicle_capacity = 0, scale = 1;
	VRP_instance &I = VRP_instance_file::readInstanceFile_SSVRPTW_CR__Candaele(carrier_file, graph_file, customers_file, vehicle_capacity, scale);
	VRP_ScenarioPool_Volatile<Scenario_SS_DS_VRPTW_CR, Solution_SS_VRPTW_CR> scenario_pool(I, 10000, SS_VRPTW_CR);
	// VRP_ScenarioPool<Scenario_SS_DS_VRPTW_CR, Solution_SS_VRPTW_CR> scenario_pool_test(I, 3, SS_VRPTW_CR);

	cout << "Scale: " << j["Scale"] << endl;
	cout << "# vehicles: " << j["NumberVehicles"] << endl;
	cout << I.toString() << endl;

	VRP_instance &I_scaled = VRP_instance_file::readInstanceFile_SSVRPTW_CR__Candaele(carrier_file, graph_file, customers_file, vehicle_capacity, j["Scale"]);
	// VRP_ScenarioPool_Volatile<Scenario_SS_DS_VRPTW_CR, Solution_SS_VRPTW_CR> scenario_pool_scaled(I_scaled, 10000, SS_VRPTW_CR);

	int i = 1;
	for (json jsol : j["_Best_Solutions_Per_Run"]) {
		cout << "Solution at run " << i++  << " / " << j["_Best_Solutions_Per_Run"].size() << endl;
		Solution_SS_VRPTW_CR s_scaled(I_scaled, R_INFTY);
		s_scaled.addRoutes(j["NumberVehicles"]);
		s_scaled.setComputeExpectedDelay();	
		s_scaled.generateInitialSolution();
		s_scaled.copyFromJSON(jsol);

		// cout << s_scaled.toStringRealVertexNumbers();	
		// SimulationResults r_scaled = scenario_pool_scaled.computeExperimentalExpectedCost(s_scaled, R_INFTY);
		// cout << "Cost: " << s_scaled.getCost() << " (" << r_scaled.avg_rejected << ") " << "\t Expected delay: " << s_scaled.getExpectedDelay() << " (" << r_scaled.avg_delay << ")" << "\t Expected #satisfied: " << s_scaled.getExpectedNumberRevealedRequests() - s_scaled.getCost() << " (" << r_scaled.avg_serviced << ")" << endl;
		// cout << endl;

		Solution_SS_VRPTW_CR s(I, R_INFTY);
		s.addRoutes(j["NumberVehicles"]);
		s.setComputeExpectedDelay();	
		s.generateInitialSolution();
		s.copyFromSol_scaleWaitingTimes(s_scaled);
		
		cout << s.toStringRealVertexNumbers();	
		SimulationResults r = scenario_pool.computeExperimentalExpectedCost(s, R_INFTY, false, false);
		cout << "Cost: " << s.getCost() << " (" << r.avg_rejected << ") " << "\t Expected delay: " << s.getExpectedDelay() << " (" << r.avg_delay << ")" << "\t Expected #satisfied: " << s.getExpectedNumberRevealedRequests() - s.getCost() << " (" << r.avg_serviced << ")" << endl;
		r = scenario_pool.computeExperimentalExpectedCost(s, R_SSVRP_R, false, false);
		cout << "Cost: " << s.getCost() << " (" << r.avg_rejected << ") " << "\t Expected delay: " << s.getExpectedDelay() << " (" << r.avg_delay << ")" << "\t Expected #satisfied: " << s.getExpectedNumberRevealedRequests() - s.getCost() << " (" << r.avg_serviced << ")" << endl;
		r = scenario_pool.computeExperimentalExpectedCost(s, R_SSVRP_R_BASIC, false, false);
		cout << "Cost: " << s.getCost() << " (" << r.avg_rejected << ") " << "\t Expected delay: " << s.getExpectedDelay() << " (" << r.avg_delay << ")" << "\t Expected #satisfied: " << s.getExpectedNumberRevealedRequests() - s.getCost() << " (" << r.avg_serviced << ")" << endl;
		cout << endl;

		// break;
	}
	


}



