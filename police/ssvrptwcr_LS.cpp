/*
Copyright 2018 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/




#include "VRP_instance.h"
#include "VRP_solution.h"
#include "VRP_scenario.h"
#include "NM.h"
#include "LS_Program.h"
#include "tools.h"
#include <queue>
#include <cmath>
#include <getopt.h>
#include <chrono>
#include <ctime>


#include "json.hpp"
using json = nlohmann::json;

using namespace std;

bool request_lt(const VRP_request& r1, const VRP_request& r2) {
	if (r1 == r2) return false;

	if (r1.revealTime < r2.revealTime)							// reveal time first
		return true;
	if (r1.revealTime == r2.revealTime) {
		if (r1.l < r2.l)										// end of time window second)
			return true;
		if (r1.l == r2.l) {
			ASSERT(r1.getVertex().getRegion() != r2.getVertex().getRegion(), "no tie breaking !");
			return r1.getVertex().getRegion() < r2.getVertex().getRegion();		// region number to break ties
		}
	}

	return false;
}



RecourseStrategy strategy;
string json_ouput_filename = "";
clock_t t;
json j_best_solutions_evo;

// VRP_ScenarioPool_Volatile<Scenario_SS_DS_VRPTW_CR, Solution_SS_VRPTW_CR> *scenario_pool = nullptr;

CallbackReturnType newBestSolution(Solution_SS_VRPTW_CR& bestSolution, double& eval) {
	UNUSED(eval);

	double elapsed_time = (double)(clock() - t) / CLOCKS_PER_SEC;

	// SimulationResults r = scenario_pool->computeExperimentalExpectedCost(bestSolution, R_INFTY);
	// cout << "Cost: " << bestSolution.getCost() << " (" << r.avg_rejected << ") " << "\t Expected delay: " << bestSolution.getExpectedDelay() << " (" << r.avg_delay << ")" << "\t Expected #satisfied: " << bestSolution.getExpectedNumberRevealedRequests() - bestSolution.getCost() << " (" << r.avg_serviced << ")" << endl;
	cout << setw(5) << setprecision(3) << elapsed_time << "\t Cost: " << bestSolution.getCost() << "\t Expected delay: " << bestSolution.getExpectedDelay() << endl;

	if (json_ouput_filename != "") {
		json j_sol = bestSolution.json_desc();
		j_sol["ExpectedRejects"] = bestSolution.getCost();
		j_sol["ExpectedDelay"] = bestSolution.getExpectedDelay();
		j_sol["ElapsedComputationTime"] = elapsed_time;
		j_best_solutions_evo.push_back(j_sol);
	}

	return NONE;
}



double evaluation_callback(Solution_SS_VRPTW_CR& solution) {

	if (not solution.isFeasible())
		return solution.getCost() + 1000000 * solution.getNumberViolations() + 10000 * solution.getWeightViolations();

	// cout << endl << "-----------------------" << endl << solution.toString() << solution.toString(true) << endl << "cost: " << solution.getCost() << "    exp delay: " << solution.getExpectedDelay() << endl;
	
	return solution.getCost() * 1000 + solution.getExpectedDelay();
}



void help() {
	cout << "Usage: ssvrptwcr_LS [--help] " << endl;
	cout << endl 
		<< "Options:" << endl 
		<< "\t--verbose(+/++) : turn output verbose" << endl
		<< "\t--help (-h) : print this help" << endl
		<< "\t--number-runs (-n) N : set the number of runs to be performed before printing avg results (default: 1)" << endl
		<< "\t--recourse-strategy (-R) RECOURSE: set the recourse strategy to be used; {R_INFTY,R_CAPA,R_CAPA_PLUS}" << endl
		<< "\t--graph-file (-g) FILE: for both STGUILLAIN and CANDAELE: the file that contains travel times (and other vehicle related stuffs for CANDAELE)" << endl
		<< "\t--carrier-file (-c) FILE: only for type CANDAELE: the carrier instance file" << endl
		<< "\t--customer-file (-C) FILE: only for type CANDAELE: the customer instance file" << endl
		<< "\t--number-vehicles (-V) N: number of vehicles to be used" << endl
		<< "\t--wait-multiple (-W) N: set the waiting time multiple to be used during optimization" << endl
		<< "\t--scale (-s) N: set the scale to be used during optimization (default: 1.0)" << endl
		<< "\t--time-dependent (-D) : turn time-dependency ON (the carrier instance file must be compatible)" << endl
		<< "\t--time-limit (-T) N: set the time limit of optimization (seconds)" << endl
		<< "\t--iter-limit (-I) N: set the maximum number of LS iterations" << endl
		<< "\t--output-json (-o) FILE_PREFIX: ouput a summary of the optimisation evolution to json files (one per run)" << endl
		<< endl;
}

int main(int argc, char **argv) {
	if (argc < 2) { help(); return 1; }

	VRP_request::request_comparison_lt = &request_lt;
	srand(time(NULL));

	const char* carrier_file = nullptr;
	const char* customers_file = nullptr;
	const char* graph_file = nullptr;
	int verbose = 0;
	int n_runs = 1;
	string recourse_strategy_str;
	int number_vehicles = -1;
	int waiting_multiple = -1;
	int time_limit = numeric_limits<int>::max(), iter_limit = numeric_limits<int>::max();
	int scale = 1;
	bool time_dependent = false;


	static struct option long_options[] = {
		{"verbose"		, no_argument		, &verbose,	1},
		{"verbose+"		, no_argument		, &verbose,	2},
		{"verbose++"	, no_argument		, &verbose,	3},
		{"help"			, no_argument		, 0, 'h'},
		{"number-runs"	, required_argument	, 0, 'n'},
		{"recourse-strategy", required_argument	, 0, 'R'},
		{"carrier-file"	, required_argument	, 0, 'c'},
		{"customer-file", required_argument	, 0, 'C'},
		{"graph-file"	, required_argument	, 0, 'g'},
		{"number-vehicles"	, required_argument	, 0, 'V'},
		{"wait-multiple"	, required_argument	, 0, 'W'},
		{"time-limit"	, required_argument	, 0, 'T'},
		{"iter-limit"	, required_argument	, 0, 'I'},
		{"scale"		, required_argument	, 0, 's'},
		{"output-json"		, required_argument	, 0, 'o'},
		{"time-dependent"	, no_argument		, 0, 'D'},
		{0, 0, 0, 0}
	};
	int c, option_index = 0;
	while ((c = getopt_long (argc, argv, "hn:R:c:C:g:V:W:T:I:s:o:D", long_options, &option_index)) != -1 ) {
		vector<string> x;
		switch (c) {
			case 'h': help(); return 1;
			case 'n': n_runs = atoi(optarg); break;
			case 'R': recourse_strategy_str.assign(optarg); break;
			case 'c': carrier_file = optarg; break;
			case 'C': customers_file = optarg; break;
			case 'g': graph_file = optarg; break;
			case 'V': number_vehicles = atoi(optarg); break;
			case 'W': waiting_multiple = atoi(optarg); break;
			case 'T': time_limit = atoi(optarg); break;
			case 'I': iter_limit = atoi(optarg); break;
			case 's': scale = atoi(optarg); break;
			case 'o': json_ouput_filename = optarg; break;
			case 'D': time_dependent = true; break;
			case '?': abort();
		}
	}
	if (carrier_file == nullptr) cout << "ERROR: if instance type is set to CANDAELE, the carrier instance file must be specified with --carrier-file (-c) FILE" << endl, abort();
	if (customers_file == nullptr) cout << "ERROR: if instance type is set to CANDAELE, the customer instance file must be specified with --customer-file (-C) FILE" << endl, abort();
	if (graph_file == nullptr) cout << "ERROR: if instance type is set to CANDAELE, the graph instance file must be specified with --graph-file (-g) FILE" << endl, abort();
	
	if (number_vehicles <= 0) cout << "ERROR: Required argument: --number-vehicles (-V) N" << endl, abort();
	if (waiting_multiple <= 0) cout << "ERROR: Required argument: --wait-multiple (-W) N" << endl, abort();
	if (time_limit == numeric_limits<int>::max() and iter_limit == numeric_limits<int>::max()) cout << "ERROR: Required argument: --time-limit (-T) N    or  --iter-limit (-I) N" << endl, abort();
	// if (time_limit > 0 and iter_limit > 0) cout << "ERROR: --time-limit  and  --iter-limit   cannot be set at the same time" << endl, abort();


	// RecourseStrategy strategy;
	if (recourse_strategy_str == "R_INFTY") 			strategy = R_INFTY;
	else if (recourse_strategy_str == "R_CAPA") 		strategy = R_CAPA;
	else if (recourse_strategy_str == "R_CAPA_PLUS") 	strategy = R_CAPA_PLUS;
	else _ASSERT_(false, "wrong recourse strategy: " << recourse_strategy_str << " (recourse strategy should be set with argument --recourse-strategy (-R) RECOURSE)");

	int vehicle_capacity = 0;
	VRP_instance &I = VRP_instance_file::readInstanceFile_SSVRPTW_CR__Candaele(string(carrier_file), string(graph_file), string(customers_file), vehicle_capacity, scale);
	if (time_dependent)
		I.setTimeDependent();
	I.setNoHorizonTW();
		
	waiting_multiple /= scale;
	_ASSERT_(waiting_multiple == abs(waiting_multiple), waiting_multiple);

	if (verbose >= 1) {
		cout << I.toString() << endl;
		cout << "#Vehicles: " << number_vehicles << " (Q=" << vehicle_capacity << ")" << endl;
		cout << "RecourseStrategy : " << recourse_strategy_str << endl;
		cout << "Wait. time multiple:\t" << setw(3) << waiting_multiple << endl;
	}

	// cout << I.toStringPotentialRequests(true);
	
	// scenario_pool = new VRP_ScenarioPool_Volatile<Scenario_SS_DS_VRPTW_CR, Solution_SS_VRPTW_CR>(I, 10000, SS_VRPTW_CR);

	// NEIGHBORHOOD MANAGER parameters
	int minIncrement = waiting_multiple;
	int maxIncrement;
	if (waiting_multiple == 1) 
		maxIncrement = ceil(I.getHorizon() / 5);
	else 
		maxIncrement = I.getHorizon()/5;


	Solution_SS_VRPTW_CR s(I, strategy);
	s.setComputeExpectedDelay();
	s.setWaitingTimeMultiple(waiting_multiple);
	s.setMinimumWaitingTime(1.0);
	int initialWaitingTime = max(waiting_multiple, (int)floor(60 / scale));
	s.setInitialWaitingTime(initialWaitingTime);
	s.addRoutes(number_vehicles);



	vector<json> j_best_sols_per_runs;
	for (int i = 0; i < n_runs; i++) {
		cout << "################# run " << i +1 << "/" << n_runs <<  " #################" << endl << endl;
		t = clock();
		

		// cout << I.toStringCoords(true) << endl;
		if (verbose >= 3) {
			cout << I.toStringVehicleTypes() << endl;
			cout << I.toString_TravelTimes(true) << endl;
			cout << I.toStringProbasTS(true) << endl << endl;
		}
		// cout << I.getExpectedNumber_appearedRequests() << endl;

		NeighborhoodManager<Solution_SS_VRPTW_CR> nm(s, SS_VRPTW_CR, I.getHorizon(), minIncrement, maxIncrement);

		// MAIN
		s.generateInitialSolution(true);

		// LS_Program_Basic<Solution_SS_VRPTW_CR,NeighborhoodManager<Solution_SS_VRPTW_CR>> lsprog;
		LS_Program_SimulatedAnnealing<Solution_SS_VRPTW_CR,NeighborhoodManager<Solution_SS_VRPTW_CR>> lsprog;
		lsprog.set_Evaluation_Callback(evaluation_callback);
		// lsprog.set_Time_Callback(printBestSolutionCost, 1.0);
		if (json_ouput_filename != "") {
			double x = 0.0;	
			newBestSolution(s, x);
		}
		lsprog.set_NewBestSolution_Callback(newBestSolution);
		lsprog.setTimeOut(time_limit);
		lsprog.run(s, nm, iter_limit, 1.0, 0.95, verbose);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)
		// lsprog.run(s, nm, iter_limit, 0, numeric_limits<int>::max(), verbose);	// args: (Solution &solution, NM &nm, int max_iter, int diversification = 10, int max_distance = 2500, bool verbose = false)		

		cout << s.toString() << "Cost: " << s.getCost() << "\t Expected delay: " << s.getExpectedDelay() << endl << endl;
		// cout << endl << s.toString(true) << endl; // << s.toStringViolations() << endl;


		if (json_ouput_filename != "") {
			json j = s.json_desc();
			j["_Evolution_Incumbents"] = j_best_solutions_evo;
			j_best_solutions_evo = {};
			j_best_sols_per_runs.push_back(j);
		}
	}


	/* Writing solution JSON file */
	if (json_ouput_filename != "") {
		cout << endl << "Writting JSON solution file...";
		ofstream json_output_file;
		json j; 
		time_t now = chrono::system_clock::to_time_t(chrono::system_clock::now());
		j["CurrentDateTime"] = ctime(& now);
		if (time_dependent)
			j["Algo"] = "LS - TimeDependent";
		else
			j["Algo"] = "LS - Non TimeDependent";
		j["NumberVehicles"] = number_vehicles;
		j["WaitingTimeMultiple"] = waiting_multiple;
		j["Scale"] = scale;
		j["Instance_Files_Graph"] = graph_file;
		j["Instance_Files_Carrier"] = carrier_file;
		j["Instance_Files_Customers"] = customers_file;
		j["OptimizationTimeout"] = time_limit;
		j["NumberOfRuns"] = n_runs;
		j["_Best_Solutions_Per_Run"] = j_best_sols_per_runs;
		json_output_file.open(json_ouput_filename, ofstream::out | ios::trunc); 
		json_output_file << j.dump(4);
		json_output_file.close();
		cout << "    Done." << endl;
	}


}



