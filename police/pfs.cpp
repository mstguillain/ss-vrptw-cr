/*
Copyright 2017 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/



/*

The Local Search algorithm described in paper:
"The Static and Stochastic VRPTW with both random Customers and Reveal Times: algorithms and recourse strategies", Saint-Guillain et al, 2017

Handles on instance files of the type described in the same paper. 
*/



#include "VRP_instance.h"
#include "VRP_solution.h"
#include "VRP_scenario.h"
#include "NM.h"
#include "LS_Program.h"
#include "tools.h"
#include <queue>
#include <cmath>
#include <ctime>	// clock()
#include <chrono>
#include <getopt.h>

#include "json.hpp"
using json = nlohmann::json;

using namespace std;

bool request_lt(const VRP_request& r1, const VRP_request& r2) {
	if (r1 == r2) return false;

	if (r1.revealTime < r2.revealTime)							// reveal time first
		return true;
	if (r1.revealTime == r2.revealTime) {
		if (r1.l < r2.l)										// end of time window second)
			return true;
		if (r1.l == r2.l) {
			ASSERT(r1.getVertex().getRegion() != r2.getVertex().getRegion(), "no tie breaking !");
			return r1.getVertex().getRegion() < r2.getVertex().getRegion();		// region number to break ties
		}
	}

	return false;
}

string json_ouput_filename = "";

RecourseStrategy strategy;
clock_t t;
json j_best_solutions_evo;


clock_t start_time;
// json j_best_solutions;
json j_last_best_sol;

CallbackReturnType newBestSolution(Solution_SS_VRPTW_CR& bestSolution, double& eval) {
	UNUSED(eval);

	j_last_best_sol = bestSolution.json_desc();
	double elapsed_time = (double)(clock() - start_time) / CLOCKS_PER_SEC;


	if (json_ouput_filename != "") {
		j_last_best_sol["ExpectedRejects"] = bestSolution.getCost();
		j_last_best_sol["ExpectedDelay"] = bestSolution.getExpectedDelay();
		j_last_best_sol["ElapsedComputationTime"] = elapsed_time;
		j_best_solutions_evo.push_back(j_last_best_sol);
	}

	return NONE;
}




double evaluation_callback(Solution_SS_VRPTW_CR& solution) {
	if (not solution.isFeasible())
		return solution.getCost() + 1000000 * solution.getNumberViolations() + 10000 * solution.getWeightViolations();
	
	return solution.getCost() * 1000 + solution.getExpectedDelay();
}




void help() {
	cout << "Usage: pfs [--help] " << endl;
	cout << endl 
		<< "Options:" << endl 
		<< "\t--verbose(+/++) : turn output verbose" << endl
		<< "\t--help (-h) : print this help" << endl
		<< "\t--update (-U) N : json file describing the update policy " << endl
		<< "\t--number-runs (-n) N : set the number of runs to be performed before printing avg results (default: 1)" << endl
		<< "\t--recourse-strategy (-R) RECOURSE: set the recourse strategy to be used; {R_INFTY,R_CAPA,R_CAPA_PLUS}" << endl
		<< "\t--graph-files-path (-g) PATH: path to the graph files, named graph_N.json, where N is the number of waiting vertices" << endl
		<< "\t--carrier-file (-c) FILE: the carrier instance file" << endl
		<< "\t--customer-file (-C) FILE: the customer instance file" << endl
		<< "\t--number-vehicles (-V) N: number of vehicles to be used" << endl
		<< "\t--vehicles-capacity (-Q) N: set the capacity of the vehicle(s); default: 0" << endl
		<< "\t--time-dependent (-D) : turn time-dependency ON (the carrier instance file must be compatible)" << endl
		<< "\t--time-limit (-T) N: set the time limit of optimization (seconds)" << endl
		<< "\t--output-json (-o) FILE: ouput a JSON summary of the optimisation processes" << endl
		<< endl;
}

int main(int argc, char **argv) {
	VRP_request::request_comparison_lt = &request_lt;
	srand(time(NULL));

	if (argc < 2) { help(); return 1; }
	
	const char* update_policy_jfile = nullptr;
	const char* carrier_file = nullptr;
	const char* customers_file = nullptr;
	const char* graph_file = nullptr;
	string recourse_strategy_str;
	int verbose = 0;
	int nb_runs = 1;
	int number_vehicles = -1;
	int vehicle_capacity = -1;
	int time_limit = -1;
	bool time_dependent = false;

	static struct option long_options[] = {
		{"verbose"		, no_argument		, &verbose,	1},
		{"verbose+"		, no_argument		, &verbose,	2},
		{"verbose++"	, no_argument		, &verbose,	3},
		{"help"			, no_argument		, 0, 'h'},
		{"update"		, required_argument	, 0, 'U'},
		{"number-runs"	, required_argument	, 0, 'n'},
		{"carrier-file"	, required_argument	, 0, 'c'},
		{"customer-file", required_argument	, 0, 'C'},
		{"graph-files-path"	, required_argument	, 0, 'g'},
		{"number-vehicles"	, required_argument	, 0, 'V'},
		{"vehicle-capacity"	, required_argument	, 0, 'Q'},
		{"recourse-strategy", required_argument	, 0, 'R'},
		{"time-limit"	, required_argument	, 0, 'T'},
		{"output-json"		, required_argument	, 0, 'o'},
		{"time-dependent"	, no_argument		, 0, 'D'},
		{0, 0, 0, 0}
	};
	int c, option_index = 0;
	while ((c = getopt_long (argc, argv, "hU:n:t:R:g:C:c:V:Q:T:o:D", long_options, &option_index)) != -1 ) {
		vector<string> x;
		switch (c) {
			case 'h': help(); return 1;
			case 'n': nb_runs = atoi(optarg); break;
			case 'U': update_policy_jfile = optarg; break;
			case 'c': carrier_file = optarg; break;
			case 'C': customers_file = optarg; break;
			case 'g': graph_file = optarg; break;
			case 'R': recourse_strategy_str.assign(optarg); break;
			case 'V': number_vehicles = atoi(optarg); break;
			case 'Q': vehicle_capacity = atoi(optarg); break;
			case 'T': time_limit = atoi(optarg); break;
			case 'o': json_ouput_filename = optarg; break;
			case 'D': time_dependent = true; break;
			case '?': abort();
		}
	}

	if (update_policy_jfile == nullptr) cout << "ERROR: the update policy file must be specified with --update (-U) FILE" << endl, abort();
	if (graph_file == nullptr) cout << "ERROR: the graph file must be specified with --graph-file (-g) FILE" << endl, abort();
	if (customers_file == nullptr) cout << "ERROR: the customer file must be specified with --customer-file (-C) FILE" << endl, abort();
	if (carrier_file == nullptr) cout << "ERROR: the carrier file must be specified with --carrier-file (-c) FILE" << endl, abort();
	if (vehicle_capacity < 0) cout << "ERROR: required argument: --vehicle-capacity (-Q) N" << endl, abort();	
	if (number_vehicles < 0) cout << "ERROR: required argument: --number-vehicles (-V) N" << endl, abort();
	if (time_limit < 0) cout << "ERROR: Required argument: --time-limit (-T) N" << endl, abort();
	

	std::ifstream update_file;
	update_file.open(update_policy_jfile);
	if (update_file.fail()) {
		std::cerr << endl << " Error: Unable to open update policy json file " << update_policy_jfile << endl;
		exit (8);
	}
	json j_update;
	update_file >> j_update;		
	update_file.close();	


	// RecourseStrategy strategy;
	if (recourse_strategy_str == "R_INFTY") 			strategy = R_INFTY;
	else if (recourse_strategy_str == "R_CAPA") 		strategy = R_CAPA;
	else if (recourse_strategy_str == "R_CAPA_PLUS") 	strategy = R_CAPA_PLUS;
	else _ASSERT_(false, "wrong recourse strategy: " << recourse_strategy_str << " (recourse strategy should be set with argument --recourse-strategy (-R) RECOURSE)");



	map< pair<int, int>, VRP_instance *> I_graph_scale;

	for (json j_config : j_update["update_policy"]) {
		int scale = j_config["scale"], n_waiting = j_config["n_waiting"];
		string graph_filename = Str(graph_file) + "/graph_" + Str(j_config["n_waiting"]) + ".json";
		cout << "Loading instance with " << n_waiting << "  , scale = " << scale << "(" << "Graph file: " << graph_filename << ")" <<  endl;
		I_graph_scale[make_pair(n_waiting, scale)] = & VRP_instance_file::readInstanceFile_SSVRPTW_CR__Candaele(string(carrier_file), string(graph_filename), string(customers_file), vehicle_capacity, scale);
		if (time_dependent)
			I_graph_scale[make_pair(n_waiting, scale)]->setTimeDependent();
		I_graph_scale[make_pair(n_waiting, scale)]->setNoHorizonTW();
	}

	vector<json> j_best_sols_per_runs;
	for (int i = 0; i < nb_runs; i++) {
		cout << "################# run " << i +1 << "/" << nb_runs <<  " #################" << endl << endl;
		j_last_best_sol.clear();
		t = clock();
		

		for (json j_config : j_update["update_policy"]) {
			int scale = j_config["scale"], wm = j_config["waiting_multiple"], n_waiting = j_config["n_waiting"];
			cout << "Scale: " << scale << "   WM: " << wm << "    |W|: " << n_waiting << endl;
			

			VRP_instance &I = * I_graph_scale[make_pair(n_waiting, scale)];

			wm /= scale;
			_ASSERT_(wm == abs(wm), wm);


			if (verbose >= 1) {
				cout << I.toString() << endl;
				cout << "#Vehicles: " << number_vehicles << " (Q=" << vehicle_capacity << ")" << endl;
				cout << "RecourseStrategy : " << recourse_strategy_str << endl;
				cout << "Wait. time multiple:\t" << setw(3) << wm << endl;
			}

			// NEIGHBORHOOD MANAGER parameters
			int minIncrement = wm;
			int maxIncrement;
			wm == 1 ? maxIncrement = ceil(I.getHorizon() / 5) : maxIncrement = I.getHorizon()/5;





			Solution_SS_VRPTW_CR s(I, strategy);
			s.setComputeExpectedDelay();
			s.setWaitingTimeMultiple(wm);
			s.setMinimumWaitingTime(1.0);
			int initialWaitingTime = max(wm, (int)floor(60 / scale));
			s.setInitialWaitingTime(initialWaitingTime);
			s.addRoutes(number_vehicles);


			// cout << I.toStringCoords(true) << endl;
			if (verbose >= 3) {
				cout << I.toStringVehicleTypes() << endl;
				cout << I.toString_TravelTimes(true) << endl;
				cout << I.toStringProbasTS(true) << endl << endl;
			}
			// cout << I.getExpectedNumber_appearedRequests() << endl;

			NeighborhoodManager<Solution_SS_VRPTW_CR> nm(s, SS_VRPTW_CR, I.getHorizon(), minIncrement, maxIncrement);

			// MAIN
			s.generateInitialSolution(true);	
			if (not j_last_best_sol.empty())
				s.copyFromJSON(j_last_best_sol);

			cout << s.toString() << endl;

			// LS_Program_Basic<Solution_SS_VRPTW_CR,NeighborhoodManager<Solution_SS_VRPTW_CR>> lsprog;
			LS_Program_SimulatedAnnealing<Solution_SS_VRPTW_CR,NeighborhoodManager<Solution_SS_VRPTW_CR>> lsprog;
			lsprog.set_Evaluation_Callback(evaluation_callback);
			if (json_ouput_filename != "") {
				double x = 0.0;	
				newBestSolution(s, x);
			}
			lsprog.set_NewBestSolution_Callback(newBestSolution);
			lsprog.setTimeOut(time_limit / j_update["update_policy"].size());
			lsprog.run(s, nm, numeric_limits<int>::max(), 1.0, 0.95, verbose);		// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)
			// lsprog.run(s, nm, numeric_limits<int>::max(), 0, numeric_limits<int>::max(), verbose);	// args: (Solution &solution, NM &nm, int max_iter, int diversification = 10, int max_distance = 2500, bool verbose = false)		

			cout << s.toString() << endl;
			// cout << endl << s.toString(true) << endl; // << s.toStringViolations() << endl;







			cout << "---------------------------" << endl << endl << endl;
		}




		






		if (json_ouput_filename != "") {
			json j = j_last_best_sol;
			j["_Evolution_Incumbents"] = j_best_solutions_evo;
			j_best_solutions_evo = {};
			j_best_sols_per_runs.push_back(j);
		}
	}







	










	/* Writing solution JSON file */
	if (json_ouput_filename != "") {
		cout << endl << "Writting JSON solution file...";
		ofstream json_output_file;
		json j; 
		time_t now = chrono::system_clock::to_time_t(chrono::system_clock::now());
		j["CurrentDateTime"] = ctime(& now);
		if (time_dependent)
			j["Algo"] = "PFS - TimeDependent";
		else
			j["Algo"] = "PFS - Non TimeDependent";
		j["UpdatePolicy"] = j_update["update_policy"];
		j["NumberVehicles"] = number_vehicles;
		j["Instance_Files_Graph"] = graph_file;
		j["Instance_Files_Carrier"] = carrier_file;
		j["Instance_Files_Customers"] = customers_file;
		j["OptimizationTimeout"] = time_limit;
		j["NumberOfRuns"] = nb_runs;
		j["_Best_Solutions_Per_Run"] = j_best_sols_per_runs;
		json_output_file.open(json_ouput_filename, ofstream::out | ios::trunc); 
		json_output_file << j.dump(4);
		json_output_file.close();
		cout << "    Done." << endl;
	}
}



