/*
Copyright 2018 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/




#include "VRP_instance.h"
#include "VRP_solution.h"
#include "VRP_scenario.h"
#include "NM.h"
#include "LS_Program.h"
#include "tools.h"
#include <queue>
#include <cmath>
#include <getopt.h>
#include <chrono>
#include <ctime>
#include <iostream>
#include <string>


#include "json.hpp"
using json = nlohmann::json;

using namespace std;

bool request_lt(const VRP_request& r1, const VRP_request& r2) {
	if (r1 == r2) return false;

	if (r1.revealTime < r2.revealTime)							// reveal time first
		return true;
	if (r1.revealTime == r2.revealTime) {
		if (r1.l < r2.l)										// end of time window second)
			return true;
		if (r1.l == r2.l) {
			ASSERT(r1.getVertex().getRegion() != r2.getVertex().getRegion(), "no tie breaking !");
			return r1.getVertex().getRegion() < r2.getVertex().getRegion();		// region number to break ties
		}
	}

	return false;
}



string getFileName(const string& s) {
	char sep = '/';
	size_t i = s.rfind(sep, s.length());
	if (i != string::npos)
		return(s.substr(i+1, s.length() - i));
	return("");
}

int main(int argc, char **argv) {
	if (argc < 3) {
		cout << "Usage: recompute_solutions solution_file.json /path/to/instance_files_dir scenarios_file" << endl;
		return 1;
	}
	const char* filename = argv[1];
	const char* instance_path_ = argv[2];
	const char* scenarios_filename = argv[3];
	// const char* newfilename = argv[2];
	std::ifstream jfile;
	jfile.open(filename);
	json j; jfile >> j; jfile.close();

	std::ifstream jfile_scenarios;
	jfile_scenarios.open(scenarios_filename);
	json j_scenarios; jfile_scenarios >> j_scenarios; jfile_scenarios.close();

	VRP_request::request_comparison_lt = &request_lt;

	// string instance_path 	= "/Users/stguillain/Dropbox/UCL/PhD/VRP/benchmarks/police";
	string instance_path 	= instance_path_;
	string graph_file 		= instance_path + "/";
	if (j["Algo"] == "PFS - Non TimeDependent" or j["Algo"] == "PFS - TimeDependent")
		graph_file += "graph_150.json";
	else
		graph_file += getFileName(j["Instance_Files_Graph"]);
	string carrier_file 	= instance_path + "/carrier/carrier_tuesday_8oct19_averages.json";
	string carrier_file_TD 	= instance_path + "/carrier_TD/carrier_tuesday_8oct19.json";
	string customers_file 	= instance_path + "/customers/" + getFileName(j["Instance_Files_Customers"]);

	cout << graph_file << endl;
	cout << carrier_file << endl;
	cout << carrier_file_TD << endl;
	cout << customers_file << endl;

	int vehicle_capacity = 0;
	int n_scenarios = 10000;

	VRP_instance &I = VRP_instance_file::readInstanceFile_SSVRPTW_CR__Candaele(carrier_file, graph_file, customers_file, vehicle_capacity, 1);
	VRP_instance &I_TD = VRP_instance_file::readInstanceFile_SSVRPTW_CR__Candaele(carrier_file_TD, graph_file, customers_file, vehicle_capacity, 1);
	I_TD.setTimeDependent();

	VRP_ScenarioPool_Volatile<Scenario_SS_DS_VRPTW_CR, Solution_SS_VRPTW_CR> scenario_pool(I, n_scenarios, SS_VRPTW_CR);
	VRP_ScenarioPool_Volatile<Scenario_SS_DS_VRPTW_CR, Solution_SS_VRPTW_CR> scenario_pool_TD(I_TD, n_scenarios, SS_VRPTW_CR);
	
	VRP_ScenarioPool<Scenario_SS_DS_VRPTW_CR, Solution_SS_VRPTW_CR> scenario_pool_2017(I, j_scenarios, SS_VRPTW_CR);
	VRP_ScenarioPool<Scenario_SS_DS_VRPTW_CR, Solution_SS_VRPTW_CR> scenario_pool_2017_TD(I_TD, j_scenarios, SS_VRPTW_CR);

	cout << "Scale: " << j["Scale"] << endl;
	cout << "# vehicles: " << j["NumberVehicles"] << endl;
	cout << I.toString() << endl;



	// double 	avg_delay_R_INFTY = 0.0, 
	double	avg_delay_R_SSVRP_R = 0.0,
			avg_delay_R_SSVRP_R___TD = 0.0,
			avg_delay_R_SSVRP_R__2017 = 0.0,
			avg_delay_R_SSVRP_R__2017_TD = 0.0;

	VRP_instance &I_scaled = VRP_instance_file::readInstanceFile_SSVRPTW_CR__Candaele(carrier_file, graph_file, customers_file, vehicle_capacity, j["_Best_Solutions_Per_Run"][0]["Scale"]);

	VRP_instance &I_scaled_TD = VRP_instance_file::readInstanceFile_SSVRPTW_CR__Candaele(carrier_file_TD, graph_file, customers_file, vehicle_capacity, j["_Best_Solutions_Per_Run"][0]["Scale"]);
	I_scaled_TD.setTimeDependent();

	int i = 1;
	for (json& jsol : j["_Best_Solutions_Per_Run"]) {

		
		// for (VRP_vertex * w : I_scaled.getWaitingVertices()) cout << w->toString(true,true) << " , ";
		// cout << endl ;
		Solution_SS_VRPTW_CR s_scaled(I_scaled, R_INFTY);
		s_scaled.addRoutes(j["NumberVehicles"]);
		s_scaled.setComputeExpectedDelay();	
		s_scaled.generateInitialSolution();
		s_scaled.copyFromJSON(jsol);

		// cout << s_scaled.toStringRealVertexNumbers();	
		// SimulationResults r_scaled = scenario_pool_scaled.computeExperimentalExpectedCost(s_scaled, R_INFTY);
		// cout << "Cost: " << s_scaled.getCost() << " (" << r_scaled.avg_rejected << ") " << "\t Expected delay: " << s_scaled.getExpectedDelay() << " (" << r_scaled.avg_delay << ")" << "\t Expected #satisfied: " << s_scaled.getExpectedNumberRevealedRequests() - s_scaled.getCost() << " (" << r_scaled.avg_serviced << ")" << endl;
		// cout << endl;

		Solution_SS_VRPTW_CR s(I, R_INFTY);
		s.addRoutes(j["NumberVehicles"]);
		s.setComputeExpectedDelay();	
		s.generateInitialSolution(true);
		s.copyFromSol_scaleWaitingTimes(s_scaled, true);

		cout << s.toStringRealVertexNumbers() << flush;	
		// cout << s.toStringRealVertexNumbers(true) << flush;		


		// TIME-DEPENDENT
		
		Solution_SS_VRPTW_CR s_scaled_TD(I_scaled_TD, R_INFTY);
		s_scaled_TD.addRoutes(j["NumberVehicles"]);
		s_scaled_TD.setComputeExpectedDelay();	
		s_scaled_TD.generateInitialSolution();
		s_scaled_TD.copyFromJSON(jsol);

		Solution_SS_VRPTW_CR s_TD(I_TD, R_INFTY);
		s_TD.addRoutes(j["NumberVehicles"]);
		s_TD.setComputeExpectedDelay();	
		s_TD.generateInitialSolution(true);
		s_TD.copyFromSol_scaleWaitingTimes(s_scaled_TD);
		// cout << s_TD.toStringRealVertexNumbers(true) << flush;		

		bool force_recompute = true;

		SimulationResults r;

		if (i == 1) {
			cout << endl << "Computing BASIC averages" << endl;

			// if (force_recompute or j["AverageDelay_BASIC"].is_null()) {
			// 	r = scenario_pool.computeExperimentalExpectedCost(s, R_SSVRP_R_BASIC, false, false);
			// 	cout << "AverageDelay_BASIC       Avg reject: " << r.avg_rejected << "\t Avg delay: "<< r.avg_delay << "\t Avg #satisfied: " << r.avg_serviced << endl;
			// 	j["AverageDelay_BASIC"] = r.avg_delay;
			// }

			if (force_recompute or j["AverageDelay_BASIC__2017"].is_null()) {
				r = scenario_pool_2017.computeExperimentalExpectedCost(s, R_SSVRP_R_BASIC, false, false);
				cout << "AverageDelay_BASIC__2017       Avg reject: " << r.avg_rejected << "\t Avg delay: "<< r.avg_delay << "\t Avg #satisfied: " << r.avg_serviced << endl;
				j["AverageDelay_BASIC__2017"] = r.avg_delay;
			}

			// if (force_recompute or j["AverageDelay_BASIC___TD"].is_null()) {
			// 	r = scenario_pool_TD.computeExperimentalExpectedCost(s_TD, R_SSVRP_R_BASIC, false, false);
			// 	cout << "AverageDelay_BASIC___TD       Avg reject: " << r.avg_rejected << "\t Avg delay: "<< r.avg_delay << "\t Avg #satisfied: " << r.avg_serviced << endl;
			// 	j["AverageDelay_BASIC___TD"] = r.avg_delay;
			// }

			if (force_recompute or j["AverageDelay_BASIC__2017_TD"].is_null()) {
				r = scenario_pool_2017_TD.computeExperimentalExpectedCost(s_TD, R_SSVRP_R_BASIC, false, false);
				cout << "AverageDelay_BASIC__2017_TD       Avg reject: " << r.avg_rejected << "\t Avg delay: "<< r.avg_delay << "\t Avg #satisfied: " << r.avg_serviced << endl;
				j["AverageDelay_BASIC__2017_TD"] = r.avg_delay;
			}
		}


		cout << "Solution at run " << i++  << " / " << j["_Best_Solutions_Per_Run"].size() << endl;



		
		// if (jsol["AverageDelay_R_INFTY"].is_null()) {
		// 	r = scenario_pool.computeExperimentalExpectedCost(*sol_simu, R_INFTY, false, false);
		// 	cout << "AverageDelay_R_INFTY    Cost: " << s.getCost() << " (" << r.avg_rejected << ") " << "\t Expected delay: " << sol_simu->getExpectedDelay() << " (" << r.avg_delay << ")" << "\t Expected #satisfied: " << sol_simu->getExpectedNumberRevealedRequests() - sol_simu->getCost() << " (" << r.avg_serviced << ")" << endl;
		// 	jsol["AverageDelay_R_INFTY"] = r.avg_delay;
		// }
		// avg_delay_R_INFTY += (double) jsol["AverageDelay_R_INFTY"];
		

		jsol["ExpectedDelay"] = s.getExpectedDelay();
		jsol["ExpectedDelay_TD"] = s_TD.getExpectedDelay();


		// //*******************************************************************************  2013-2016

		// if (force_recompute or jsol["AverageDelay_R_SSVRP_R"].is_null()) {
		// 	r = scenario_pool.computeExperimentalExpectedCost(s, R_SSVRP_R, false, false);
		// 	cout << "AverageDelay_R_SSVRP_R   Avg reject: " << r.avg_rejected << "\t Avg delay: "<< r.avg_delay << "\t Avg #satisfied: " << r.avg_serviced << endl;
		// 	jsol["AverageDelay_R_SSVRP_R"] = r.avg_delay;
		// }
		// avg_delay_R_SSVRP_R += (double) jsol["AverageDelay_R_SSVRP_R"];

		//*******************************************************************************  2017

		if (force_recompute or jsol["AverageDelay_R_SSVRP_R__2017"].is_null()) {
			r = scenario_pool_2017.computeExperimentalExpectedCost(s, R_SSVRP_R, false, false);
			cout << "AverageDelay_R_SSVRP_R__2017   Avg reject: " << r.avg_rejected << "\t Avg delay: "<< r.avg_delay << "\t Avg #satisfied: " << r.avg_serviced << endl;
			jsol["AverageDelay_R_SSVRP_R__2017"] = r.avg_delay;
		}
		avg_delay_R_SSVRP_R__2017 += (double) jsol["AverageDelay_R_SSVRP_R__2017"];

		// //*******************************************************************************  2013-2016 - TD

		// if (force_recompute or jsol["AverageDelay_R_SSVRP_R___TD"].is_null()) {
		// 	r = scenario_pool_TD.computeExperimentalExpectedCost(s_TD, R_SSVRP_R, false, false);
		// 	cout << "AverageDelay_R_SSVRP_R___TD   Avg reject: " << r.avg_rejected << "\t Avg delay: "<< r.avg_delay << "\t Avg #satisfied: " << r.avg_serviced << endl;
		// 	jsol["AverageDelay_R_SSVRP_R___TD"] = r.avg_delay;
		// }
		// avg_delay_R_SSVRP_R___TD += (double) jsol["AverageDelay_R_SSVRP_R___TD"];

		//*******************************************************************************  2017 - TD

		cout << endl;
		if (force_recompute or jsol["AverageDelay_R_SSVRP_R__2017_TD"].is_null()) {
			r = scenario_pool_2017_TD.computeExperimentalExpectedCost(s_TD, R_SSVRP_R, false, false);
			cout << "AverageDelay_R_SSVRP_R__2017_TD   Avg reject: " << r.avg_rejected << "\t Avg delay: "<< r.avg_delay << "\t Avg #satisfied: " << r.avg_serviced << endl;
			jsol["AverageDelay_R_SSVRP_R__2017_TD"] = r.avg_delay;
		}
		avg_delay_R_SSVRP_R__2017_TD += (double) jsol["AverageDelay_R_SSVRP_R__2017_TD"];
		



		cout << endl << "-------------------------------" << endl;

		// break;
	}

	// j["AverageDelay_R_SSVRP_R"] = avg_delay_R_SSVRP_R / (double)j["_Best_Solutions_Per_Run"].size();
	// j["AverageDelay_R_SSVRP_R_TD"] = avg_delay_R_SSVRP_R___TD / (double)j["_Best_Solutions_Per_Run"].size();
	j["AverageDelay_R_SSVRP_R__2017"] 	= avg_delay_R_SSVRP_R__2017 / (double)j["_Best_Solutions_Per_Run"].size();
	j["AverageDelay_R_SSVRP_R__2017_TD"] 	= avg_delay_R_SSVRP_R__2017_TD / (double)j["_Best_Solutions_Per_Run"].size();
	
	
	cout << endl << "Updating JSON solution file...";
	ofstream json_output_file;
	json_output_file.open(filename, ofstream::out | ios::trunc); 
	json_output_file << j.dump(4);
	json_output_file.close();
	cout << "    Done." << endl;
	

}



