/*
Copyright 2018 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/




#include "VRP_instance.h"
#include "VRP_solution.h"
#include "VRP_scenario.h"
#include "NM.h"
#include "LS_Program.h"
#include "tools.h"
#include <queue>
#include <cmath>
#include <getopt.h>
#include <chrono>
#include <ctime>


#include "json.hpp"
using json = nlohmann::json;

using namespace std;

bool request_lt(const VRP_request& r1, const VRP_request& r2) {
	if (r1 == r2) return false;

	if (r1.revealTime < r2.revealTime)							// reveal time first
		return true;
	if (r1.revealTime == r2.revealTime) {
		if (r1.l < r2.l)										// end of time window second)
			return true;
		if (r1.l == r2.l) {
			ASSERT(r1.getVertex().getRegion() != r2.getVertex().getRegion(), "no tie breaking !");
			return r1.getVertex().getRegion() < r2.getVertex().getRegion();		// region number to break ties
		}
	}

	return false;
}






void help() {
	cout << "Usage: ssvrptwcr_LS [--help] " << endl;
	cout << endl 
		<< "Options:" << endl 
		<< "\t--verbose(+/++) : turn output verbose" << endl
		<< "\t--help (-h) : print this help" << endl
		<< "\t--recourse-strategy (-R) RECOURSE: set the recourse strategy to be used; {R_INFTY,R_CAPA,R_CAPA_PLUS}" << endl
		<< "\t--graph-file (-g) FILE: for both STGUILLAIN and CANDAELE: the file that contains travel times (and other vehicle related stuffs for CANDAELE)" << endl
		<< "\t--carrier-file (-c) FILE: only for type CANDAELE: the carrier instance file" << endl
		<< "\t--customer-file (-C) FILE: only for type CANDAELE: the customer instance file" << endl
		<< "\t--number-vehicles (-V) N: number of vehicles to be used" << endl
		<< "\t--wait-multiple (-W) N: set the waiting time multiple to be used during optimization" << endl
		<< "\t--scale (-s) N: set the scale to be used during optimization (default: 1.0)" << endl
		<< "\t--time-dependent (-D) : turn time-dependency ON (the carrier instance file must be compatible)" << endl
		<< endl;
}

int main(int argc, char **argv) {
	if (argc < 2) { help(); return 1; }

	VRP_request::request_comparison_lt = &request_lt;
	// srand(time(NULL));

	const char* carrier_file = nullptr;
	const char* customers_file = nullptr;
	const char* graph_file = nullptr;
	int verbose = 0;
	string recourse_strategy_str;
	int number_vehicles = -1;
	int waiting_time_multiple = -1;
	int scale = 1;
	RecourseStrategy strategy;
	bool time_dependent = false;


	static struct option long_options[] = {
		{"verbose"			, no_argument		, &verbose,	1},
		{"verbose+"			, no_argument		, &verbose,	2},
		{"verbose++"		, no_argument		, &verbose,	3},
		{"help"				, no_argument		, 0, 'h'},
		{"recourse-strategy", required_argument	, 0, 'R'},
		{"carrier-file"		, required_argument	, 0, 'c'},
		{"customer-file"	, required_argument	, 0, 'C'},
		{"graph-file"		, required_argument	, 0, 'g'},
		{"number-vehicles"	, required_argument	, 0, 'V'},
		{"wait-multiple"	, required_argument	, 0, 'W'},
		{"scale"			, required_argument	, 0, 's'},
		{"time-dependent"	, no_argument		, 0, 'D'},
		{0, 0, 0, 0}
	};
	int c, option_index = 0;
	while ((c = getopt_long (argc, argv, "hR:c:C:g:V:W:s:D", long_options, &option_index)) != -1 ) {
		vector<string> x;
		switch (c) {
			case 'h': help(); return 1;
			case 'R': recourse_strategy_str.assign(optarg); break;
			case 'c': carrier_file = optarg; break;
			case 'C': customers_file = optarg; break;
			case 'g': graph_file = optarg; break;
			case 'V': number_vehicles = atoi(optarg); break;
			case 'W': waiting_time_multiple = atoi(optarg); break;
			case 's': scale = atoi(optarg); break;
			case 'D': time_dependent = true; break;
			case '?': abort();
		}
	}
	if (carrier_file == nullptr) cout << "ERROR: if instance type is set to CANDAELE, the carrier instance file must be specified with --carrier-file (-c) FILE" << endl, abort();
	if (customers_file == nullptr) cout << "ERROR: if instance type is set to CANDAELE, the customer instance file must be specified with --customer-file (-C) FILE" << endl, abort();
	if (graph_file == nullptr) cout << "ERROR: if instance type is set to CANDAELE, the graph instance file must be specified with --graph-file (-g) FILE" << endl, abort();
	
	if (number_vehicles <= 0) cout << "ERROR: Required argument: --number-vehicles (-V) N" << endl, abort();
	if (waiting_time_multiple <= 0) cout << "ERROR: Required argument: --wait-multiple (-W) N" << endl, abort();
	


	// RecourseStrategy strategy;
	if (recourse_strategy_str == "R_INFTY") 			strategy = R_INFTY;
	else if (recourse_strategy_str == "R_CAPA") 		strategy = R_CAPA;
	else if (recourse_strategy_str == "R_CAPA_PLUS") 	strategy = R_CAPA_PLUS;
	else _ASSERT_(false, "wrong recourse strategy: " << recourse_strategy_str << " (recourse strategy should be set with argument --recourse-strategy (-R) RECOURSE)");

	int vehicle_capacity = 0;
	VRP_instance &I = VRP_instance_file::readInstanceFile_SSVRPTW_CR__Candaele(string(carrier_file), string(graph_file), string(customers_file), vehicle_capacity, scale);
	// VRP_instance &I = VRP_instance_file::readInstanceFile_DS_VRPTW__Candaele(string(carrier_file), string(graph_file), string(customers_file), vehicle_capacity);
	
	if (time_dependent)
		I.setTimeDependent();
	// I.print_TD_bins();

	waiting_time_multiple /= scale;
	_ASSERT_(waiting_time_multiple == abs(waiting_time_multiple), waiting_time_multiple);

	if (verbose >= 1) {
		cout << I.toString() << endl;
		cout << "#Vehicles: " << number_vehicles << " (Q=" << vehicle_capacity << ")" << endl;
		cout << "RecourseStrategy : " << recourse_strategy_str << endl;
		cout << "Wait. time multiple:\t" << setw(3) << waiting_time_multiple << endl;
	}
	
	// int tu = 415;
	// const VRP_VehicleType& veh_t = I.getVehicleType();
	// for (const VRP_vertex * v1 : I.getOnlineVertices()) {
	// 	for (const VRP_vertex * v2 : I.getOnlineVertices()) {
	// // for (const VRP_vertex * v1 : I.getRandomCustomerVertices()) {
	// // 	for (const VRP_vertex * v2 : I.getRandomCustomerVertices()) {
	// 		double tt = veh_t.travelTime(*v1, *v2, tu);
	// 		double dep_t = veh_t.departureTime(*v1, *v2, tu);
	// 		double tt_ = veh_t.travelTime(*v1, *v2, dep_t);
	// 		cout << v1->toStringRealVertexNumbers() << ":" << *v1 <<  " -> " << v2->toStringRealVertexNumbers() << ":" << *v2 << " : departure time to TU " << tu << " = " << dep_t << "   traveltime from " << dep_t << " = " << tt_ <<       "  -----> " << dep_t + tt_ - tu << endl;
	// 		double dep_t_ = veh_t.departureTime(*v1, *v2, tu + tt);
	// 		cout << v1->toStringRealVertexNumbers() << ":" << *v1 <<  " -> " << v2->toStringRealVertexNumbers() << ":" << *v2 << " : travel    time at TU " << tu << " = " << tt <<    " : departure   for " << tu + tt << " = " << dep_t_ <<  "  -----> " << dep_t_ - tu << endl;

	// 	}
	// 	break;
	// }

	// int r1 = 146, r2 = 96;
	// double tt = veh_t.travelTime(r1, r2, tu);
	// // double dep_t = veh_t.departureTime(r1, r2, tu);
	// // double tt_ = veh_t.travelTime(r1, r2, dep_t);
	// // cout << endl << r1 << " -> " << r2 << " : departure time to TU " << tu << " = " << dep_t << "   traveltime from " << dep_t << " = " << tt_ <<       "  -----> " << dep_t + tt_ - tu << endl << endl;
	// double dep_t_ = veh_t.departureTime(r1, r2, tu + tt);
	// cout << endl << r1 << " -> " << r2 << " : travel    time at TU " << tu << " = " << tt <<    " : departure   for " << tu + tt << " = " << dep_t_ <<  "  -----> " << dep_t_ + tt - tu << endl;
	





	Solution_SS_VRPTW_CR s(I, strategy);
	s.setComputeExpectedDelay();
	s.setWaitingTimeMultiple(waiting_time_multiple);
	s.setMinimumWaitingTime(1.0);
	int initialWaitingTime = max(waiting_time_multiple, (int)floor(60 / scale));
	s.setInitialWaitingTime(initialWaitingTime);
	s.addRoutes(number_vehicles);

	cout << s.toString() << endl;

	// // cout << I.toStringCoords(true) << endl;
	// if (verbose >= 3) {
	// 	cout << I.toStringVehicleTypes() << endl;
	// 	cout << I.toString_TravelTimes(true) << endl;
	// 	cout << I.toStringProbasTS(true) << endl << endl;
	// }
	// // cout << I.getExpectedNumber_appearedRequests() << endl;



	// MAIN
	// s.generateInitialSolution(false);
	// cout << s.toString() << endl;
	// cout << s.toString(true) << endl;

	s.generateInitialSolution(true);


	VRP_solution::SolutionElement w_loc, pos;
	// pos.type = VRP_solution::SolutionElementType::POSITION; pos.route = 1; pos.pos = 1;
	// w_loc.type = VRP_solution::SolutionElementType::WAITING_LOC; w_loc.region = 134;
	// s.insertWaitingVertex(w_loc, pos);
	// pos.type = VRP_solution::SolutionElementType::VERTEX; s.setWaitingTime(pos, 300);


	// pos.type = VRP_solution::SolutionElementType::POSITION; pos.route = 2; pos.pos = 1;
	// w_loc.type = VRP_solution::SolutionElementType::WAITING_LOC; w_loc.region = 131;
	// s.insertWaitingVertex(w_loc, pos);
	// pos.type = VRP_solution::SolutionElementType::VERTEX; s.setWaitingTime(pos, 240);


	// pos.type = VRP_solution::SolutionElementType::POSITION; pos.route = 2; pos.pos = 2;
	// w_loc.type = VRP_solution::SolutionElementType::WAITING_LOC; w_loc.region = 75;
	// s.insertWaitingVertex(w_loc, pos);
	// pos.type = VRP_solution::SolutionElementType::VERTEX; s.setWaitingTime(pos, 60);

	// pos.type = VRP_solution::SolutionElementType::POSITION; pos.route = 1; pos.pos = 1;
	// w_loc.type = VRP_solution::SolutionElementType::WAITING_LOC; w_loc.region = 140;
	// s.insertWaitingVertex(w_loc, pos);
	// pos.type = VRP_solution::SolutionElementType::VERTEX; s.setWaitingTime(pos, 60);


	pos.type = VRP_solution::SolutionElementType::POSITION; pos.route = 1; pos.pos = 1;
	w_loc.type = VRP_solution::SolutionElementType::WAITING_LOC; w_loc.region = 141;
	s.insertWaitingVertex(w_loc, pos);
	pos.type = VRP_solution::SolutionElementType::VERTEX; s.setWaitingTime(pos, 300);

	cout << s.toString() << endl;
	cout << s.toString(true) << endl;
	cout << s.getCost() << endl << s.getExpectedDelay() << endl ;
	

	// s.print_requestsAssignmentOrdering();


}













