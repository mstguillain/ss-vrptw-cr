#include "VRP_instance.h"
#include "VRP_solution.h"
#include "VRP_scenario.h"
#include "NM.h"
#include "LS_Program.h"
#include "tools.h"
#include <queue>
#include <cmath>
#include <limits>
#include <map>
#include <vector>
#include <getopt.h>

using namespace std;

int n_sol = 0;

bool request_lt(const VRP_request& r1, const VRP_request& r2) {
	if (r1 == r2) return false;

	if (r1.revealTime < r2.revealTime)							// reveal time first
		return true;
	if (r1.revealTime == r2.revealTime) {
		if (r1.l < r2.l)										// end of time window second)
			return true;
		if (r1.l == r2.l) {
			ASSERT(r1.getVertex().getRegion() != r2.getVertex().getRegion(), "no tie breaking !");
			return r1.getVertex().getRegion() < r2.getVertex().getRegion();		// region number to break ties
		}
	}

	return false;
}





void help() {
	cout << "Usage: ssvrptwcr_LS [--help] " << endl;
	cout << endl 
		<< "Options:" << endl 
		<< "\t--verbose(+/++) : turn output verbose" << endl
		<< "\t--help (-h) : print this help" << endl
		<< "\t--type-instance (-t) TYPE: specifies the type of instance files provided; either STGUILLAIN or CANDAELE" << endl

		<< "\t--instance-file (-I) FILE: the instance main file (STGUILLAIN)" << endl
		<< "\t--graph-file (-g) FILE: the file that contains travel times (and other stuffs) - (STGUILLAIN & CANDAELE)" << endl
		<< "\t--carrier-file (-c) FILE: the carrier instance file (CANDAELE)" << endl
		<< "\t--customers-file (-C) FILE: the customers instance file (CANDAELE)" << endl

		<< "\t--JSON-solutions-file (-j) FILE: the JSON file that contains the sequence of solutions " << endl
		// << "\t--recourse-strategy (-R) RECOURSE: set the recourse strategy to be used; {R_INFTY,R_CAPA,R_CAPA_PLUS}" << endl
		// << "\t--scale (-s) N: set the scale to be used during optimization (default: 1.0)" << endl
		<< "\t--output-json (-o) FILE: ouput JSON file to (over)write" << endl
		<< endl;
}


int main(int argc, char **argv) {
	VRP_request::request_comparison_lt = &request_lt;
	srand(time(NULL));

	if (argc < 2) { help(); return 1; }

	const char* json_solutions_file = nullptr;
	const char* instance_file = nullptr;
	const char* carrier_file = nullptr;
	const char* customers_file = nullptr;
	const char* graph_file = nullptr;
	int verbose = 0;
	string instance_type;
	string json_ouput_filename = "";
	// int scale = -1;
	// string recourse_strategy_str;
	

	static struct option long_options[] = {
		{"verbose"				, no_argument		, &verbose,	1},
		{"verbose+"				, no_argument		, &verbose,	2},
		{"verbose++"			, no_argument		, &verbose,	3},
		{"help"					, no_argument		, 0, 'h'},
		{"type-instance"		, required_argument	, 0, 't'},

		{"JSON-solutions-file"	, required_argument	, 0, 'j'},
		{"instance-file"		, required_argument	, 0, 'I'},
		{"carrier-file"			, required_argument	, 0, 'c'},
		{"customers-file"		, required_argument	, 0, 'C'},
		{"graph-file"			, required_argument	, 0, 'g'},

		// {"recourse-strategy"	, required_argument	, 0, 'R'},
		// {"scale"				, required_argument	, 0, 's'},
		{"output-json"			, required_argument	, 0, 'o'},
		{0, 0, 0, 0}
	};
	int c, option_index = 0;
	while ((c = getopt_long (argc, argv, "ht:j:I:c:C:g:o:", long_options, &option_index)) != -1 ) {
		switch (c) {
			case 'h': help(); return 1;
			case 't': instance_type.assign(optarg); break;

			case 'j': json_solutions_file = optarg; break;
			case 'I': instance_file = optarg; break;
			case 'c': carrier_file = optarg; break;
			case 'C': customers_file = optarg; break;
			case 'g': graph_file = optarg; break;

			// case 'R': recourse_strategy_str.assign(optarg); break;
			// case 's': scale = atoi(optarg); break;
			case 'o': json_ouput_filename = optarg; break;
			case '?': abort();
		}
	}
	
	if (instance_type != "STGUILLAIN" && instance_type != "CANDAELE") cout << "ERROR: argument --type-instance must be set to either STGUILLAIN or CANDAELE." << endl, abort();
	if (instance_type == "STGUILLAIN") {
		if (instance_file == nullptr) cout << "ERROR: if instance type is set to STGUILLAIN, the instance file must be specified with --instance-file (-I) FILE" << endl, abort();
		if (graph_file == nullptr) cout << "ERROR: if instance type is set to STGUILLAIN, the travel times instance file must be specified with --graph-file (-g) FILE" << endl, abort();
	}
	if (instance_type == "CANDAELE") {
		if (carrier_file == nullptr) cout << "ERROR: if instance type is set to CANDAELE, the carrier instance file must be specified with --carrier-file (-c) FILE" << endl, abort();
		if (customers_file == nullptr) cout << "ERROR: if instance type is set to CANDAELE, the customer instance file must be specified with --customer-file (-C) FILE" << endl, abort();
		if (graph_file == nullptr) cout << "ERROR: if instance type is set to CANDAELE, the graph instance file must be specified with --graph-file (-g) FILE" << endl, abort();
	}
	if (json_solutions_file == nullptr) cout << "ERROR: the solutions file, in JSON format, must be specified with --JSON-solutions-file (-j) FILE" << endl, abort();
	
	// if (scale < 0) cout << "ERROR: the scale must be specified with --scale (-s) N" << endl, abort();




	std::ifstream jfile;
	jfile.open(json_solutions_file);
	json j;	jfile >> j; jfile.close();

	_ASSERT_(! j["NumberVehicles"].is_null() && (int)j["NumberVehicles"] > 0, "");
	_ASSERT_(! j["VehicleCapacity"].is_null(), "");
	_ASSERT_(! j["Solutions"].is_null(), "");
	_ASSERT_(! j["Algo"].is_null(), "");

	cout << "Algorithm: " << j["Algo"] << endl;

	RecourseStrategy strategy = NOT_SET;
	if (j["Algo"] == "LS") {
		_ASSERT_(! j["RecourseStrategy"].is_null(), "");
		if (j["RecourseStrategy"] == "R_INFTY") 			strategy = R_INFTY;
		else if (j["RecourseStrategy"] == "R_CAPA") 		strategy = R_CAPA;
		else if (j["RecourseStrategy"] == "R_CAPA_PLUS") 	strategy = R_CAPA_PLUS;
		else _ASSERT_(false, "wrong recourse strategy: " << j["RecourseStrategy"]);
	}
	else strategy = R_CAPA;

	vector<int> scales;
	map<int, VRP_instance*> instance_at_scale;
	map<int, Solution_SS_VRPTW_CR*> solution_at_scale;

	if (j["Algo"] == "LS") {
		_ASSERT_(! j["Scale"].is_null(), "");
		scales.push_back(j["Scale"]);
		if (instance_type == "STGUILLAIN") {
			instance_at_scale[j["Scale"]] = & VRP_instance_file::readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs(graph_file, instance_file, j["VehicleCapacity"], j["Scale"]);
			if (j["Scale"] != 1)
				instance_at_scale[1] = & VRP_instance_file::readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs(graph_file, instance_file, j["VehicleCapacity"], 1);
		}
		if (instance_type == "CANDAELE") {
			_ASSERT_(j["Scale"] == 1, "Not implemented yet");
			instance_at_scale[j["Scale"]] = & VRP_instance_file::readInstanceFile_SSVRPTW_CR__Candaele(carrier_file, graph_file, customers_file, j["VehicleCapacity"]);
			if (j["Scale"] != 1)
				_ASSERT_(false, "Not implemented yet");
		}

	} else if (j["Algo"] == "LS_succ") {
		_ASSERT_(! j["Scales"].is_null(), "");
		for (int scale : j["Scales"]) {
			scales.push_back(scale);
			if (instance_type == "STGUILLAIN")
				instance_at_scale[scale] = & VRP_instance_file::readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs(graph_file, instance_file, j["VehicleCapacity"], scale);
			if (instance_type == "CANDAELE")
				_ASSERT_(false, "Not implemented yet");
		}
	} else _ASSERT_(false, "");

	
	

	VRP_instance & I = * instance_at_scale[scales.front()];
	if (instance_type == "STGUILLAIN")
		cout << endl << endl << "Instance files: " << instance_type << " " << graph_file << endl;
	if (instance_type == "CANDAELE")
		cout << endl << endl << "Instance files: " << carrier_file << " " << graph_file << " " << customers_file << endl;
	cout << I.toString() << endl;
	cout << "#Vehicles: " << j["NumberVehicles"] << " (" << I.getVehicleType().toString() << ")" << endl;

	for (int scale : scales) {
		solution_at_scale[scale] = new Solution_SS_VRPTW_CR(* instance_at_scale[scale], strategy);
		solution_at_scale[scale]->addRoutes(j["NumberVehicles"]);
		solution_at_scale[scale]->generateInitialSolution(true);
	}

	Solution_SS_VRPTW_CR sol_scale1(* instance_at_scale[1], R_CAPA);
	sol_scale1.addRoutes(j["NumberVehicles"]);
	sol_scale1.generateInitialSolution(true);

	Solution_SS_VRPTW_CR sol_scale2(* instance_at_scale[2], R_CAPA);
	sol_scale2.addRoutes(j["NumberVehicles"]);
	sol_scale2.generateInitialSolution(true);

	Solution_SS_VRPTW_CR sol_scale5(* instance_at_scale[5], R_CAPA);
	sol_scale5.addRoutes(j["NumberVehicles"]);
	sol_scale5.generateInitialSolution(true);

	// Solution_SS_VRPTW_CR sol_scale5(* instance_at_scale[10], R_CAPA);
	// sol_scale10.addRoutes(j["NumberVehicles"]);
	// sol_scale10.generateInitialSolution(true);

	int i = 1;
	for (json& j_sol : j["Solutions"]) {
		cout << "Solution " << i++ << " / " << j["Solutions"].size() << "..." << endl;
		_ASSERT_(! j_sol["Scale"].is_null(), "");
		Solution_SS_VRPTW_CR &s = * solution_at_scale[(int)j_sol["Scale"]];
		s.setRecourseStrategy(R_CAPA);
		s.copyFromJSON(j_sol);
		if (! j_sol["ElapsedComputationTime"].is_null())
			cout << "CPU time: " << setw(5) << j_sol["ElapsedComputationTime"] << "\t";
		cout << "Solution:     (scale: " << j_sol["Scale"] << ")" << endl << s.toString(false);
		cout << "Cost: \t\t\t" << s.getCost() << endl;
		_ASSERT_(abs(s.getCost() - (double)j_sol["Cost"]) < 0.000001, s.getCost() << " ≠ " << j_sol["Cost"]);

		if (j_sol["CostR+"].is_null()) {
			s.setRecourseStrategy(R_CAPA_PLUS);
			cout << "Cost R+: \t\t" << s.getCost() << endl;
			j_sol["CostR+"] = s.getCost();
		}

		if (j_sol["CostS1"].is_null()) {
			sol_scale1.copyFromSol_scaleWaitingTimes(s);
			cout << "Cost Scale 1: \t\t" << sol_scale1.getCost() << endl;
			j_sol["CostS1"] = sol_scale1.getCost();
		}

		if (j_sol["CostR+S1"].is_null()) {
			sol_scale1.copyFromSol_scaleWaitingTimes(s);
			sol_scale1.setRecourseStrategy(R_CAPA_PLUS);
			cout << "Cost R+ Scale 1: \t" << sol_scale1.getCost() << endl << endl;
			j_sol["CostR+S1"] = sol_scale1.getCost();
		}

		_ASSERT_(! j_sol["Scale"].is_null(), "");
		int scale = (int) j_sol["Scale"];

		if (scale != 2 && j_sol["CostS2"].is_null()) {
			sol_scale2.copyFromSol_scaleWaitingTimes(s);
			cout << "Cost Scale 2: \t\t" << sol_scale2.getCost() << endl;
			j_sol["CostS2"] = sol_scale2.getCost();
		}

		if (scale != 5 && j_sol["CostS5"].is_null()) {
			sol_scale5.copyFromSol_scaleWaitingTimes(s);
			cout << "Cost Scale 5: \t\t" << sol_scale5.getCost() << endl;
			j_sol["CostS5"] = sol_scale5.getCost();
		}
	}


	if (json_ouput_filename != "") {
		cout << endl << "Writting JSON summary file...";
		ofstream json_output_file;
		json_output_file.open(json_ouput_filename, ofstream::out | ios::trunc); 
		json_output_file << j.dump(4);
		json_output_file.close();
		cout << "    Done." << endl;
	}
	// cout << j.dump(4) << endl;
}













