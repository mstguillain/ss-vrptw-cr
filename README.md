# README #

This repository contains implementations of SS-VRPTW-CR solvers. 

Everything here is based on the [VRPlib library](https://bitbucket.org/mstguillain/vrplib).
The library contains the object oriented representations of a VRP solutions and its constraints, 
together with complex evaluation functions in case of Dyanmic and/or Stochastic extensions.

In particular, solutions for the SS-VRTPW-CR (see **[1]**, **[2]**) can be represented, and their expected cost 
under various recourse strategies can be efficiently computed. 

The programs writen in this repository are simple constructions based on the VRPlib API. 


---


Copyright 2017 Michael Saint-Guillain.

The library and programs are under GNU Lesser General Public License (LGPL) v3.

**Contact**: Michael Saint-Guillain *<m dot stguillain at famous google mail dot com>*

---


**[1]** Saint-Guillain, M., Solnon, C., Deville, Y., 2017. *The Static and Stochastic VRP with Time Windows and both random Customers and Reveal Times.* In: EvoSTOC 2017. Springer International Publishing, pp. 110�127.   
[_Download link_](http://becool.info.ucl.ac.be/biblio/static-and-stochastic-vrp-time-windows-and-both-random-customers-and-reveal-times)

**[2]** Saint-Guillain, M., Solnon, C., Deville, Y., 2017. *The Static and Stochastic VRPTW with both random Customers and Reveal Times: algorithms and recourse strategies.*   
_To be published_