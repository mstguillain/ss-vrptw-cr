import argparse
import json


###### ARGUMENTS ############
parser = argparse.ArgumentParser()

parser.add_argument("--solutions", required=True, help="JSON file containing the solutions", type=str)

args = parser.parse_args()
##############################

f = open(args.solutions)

j = json.load(f)

print("ComputationTime\t CostR+S1\t CostS1\t CostS2\t CostS5\t ")
for j_sol in j["Solutions"]:
	if j_sol["Scale"] == 1:
		costS1 = j_sol["Cost"]
	if j_sol["Scale"] == 2:
		costS2 = j_sol["Cost"]
	if j_sol["Scale"] == 5:
		costS5 = j_sol["Cost"]
	
	if ("CostS1" in j_sol) :
		costS1 = j_sol["CostS1"]
	if ("CostS2" in j_sol) :
		costS2 = j_sol["CostS2"]
	if ("CostS5" in j_sol) :
		costS5 = j_sol["CostS5"]

	if ("CostR+S1" in j_sol) :
		costRplusS1 = j_sol["CostR+S1"]
	else : 
		costRplusS1 = 0.0

	# print(str(j_sol["ElapsedComputationTime"]) + "\t " + str(costRplusS1) + "\t " + str(costS1) + "\t " + str(costS2) + "\t " + str(costS5))
	print('{:>10.2f}      {:>10.4f}  {:>10.4f}  {:>10.4f}  {:>10.4f}'.format(j_sol["ElapsedComputationTime"], costRplusS1, costS1, costS2, costS5) )

print("DEVIATIONS from R+S1:")
print()

def deviation(costFrom, cost):
	# if cost == float("Inf"):
	# 	return float("NaN")
	return min(99.99, 100 * (cost - costFrom) / costFrom);

print("ComputationTime\t %CostS1\t %CostS2\t %CostS5\t ")
for j_sol in j["Solutions"]:
	if j_sol["Scale"] == 1:
		costS1 = j_sol["Cost"]
	if j_sol["Scale"] == 2:
		costS2 = j_sol["Cost"]
	if j_sol["Scale"] == 5:
		costS5 = j_sol["Cost"]
	
	if ("CostS1" in j_sol) :
		costS1 = j_sol["CostS1"]
	if ("CostS2" in j_sol) :
		costS2 = j_sol["CostS2"]
	if ("CostS5" in j_sol) :
		costS5 = j_sol["CostS5"]

	if ("CostR+S1" in j_sol) :
		costRplusS1 = j_sol["CostR+S1"]
	else : 
		costRplusS1 = 0.0

	# print(str(j_sol["ElapsedComputationTime"]) + "\t " + str(deviation(costRplusS1,costS1)) + "\t " + str(deviation(costRplusS1,costS2)) + "\t " + str(deviation(costRplusS1,costS5)))
	print('{:>10.2f}      {:>10.2f}  {:>10.2f}  {:>10.2f}'.format(j_sol["ElapsedComputationTime"], deviation(costRplusS1,costS1), deviation(costRplusS1,costS2), deviation(costRplusS1,costS5)) )


print("ComputationTime\t %CostS2\t %CostS5\t ")
for j_sol in j["Solutions"]:
	if j_sol["Scale"] == 1:
		costS1 = j_sol["Cost"]
	if j_sol["Scale"] == 2:
		costS2 = j_sol["Cost"]
	if j_sol["Scale"] == 5:
		costS5 = j_sol["Cost"]
	
	if ("CostS1" in j_sol) :
		costS1 = j_sol["CostS1"]
	if ("CostS2" in j_sol) :
		costS2 = j_sol["CostS2"]
	if ("CostS5" in j_sol) :
		costS5 = j_sol["CostS5"]

	if ("CostR+S1" in j_sol) :
		costRplusS1 = j_sol["CostR+S1"]
	else : 
		costRplusS1 = 0.0

	print('{:>10.2f}      {:>10.2f}  {:>10.2f}'.format(j_sol["ElapsedComputationTime"], deviation(costS1, costS2), deviation(costS1, costS5)) )






