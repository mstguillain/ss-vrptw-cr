#used in http://gnuplot.respawned.com/


#### *** FIRST PLOT *** ####
# Scale font and line width (dpi) by changing the size! It will always display stretched.
set terminal svg size 400,300 enhanced fname 'arial'  fsize 10 butt solid
set output 'out.svg'

set termoption dash

filename='data.txt'

set key inside top left
set ylabel 'Deviation (%)'
set xlabel 'Time (s)'

set xrange [1:10800]
set yrange [0:50]

set logscale x

plot  \
    filename using 1:3 title 'R^{q} scale 5' with lines lt 3,  \
    filename using 1:2 title 'R^{q} scale 2' with lines lt 19




#### *** SECOND PLOT *** ####
# Scale font and line width (dpi) by changing the size! It will always display stretched.
set terminal svg size 400,300 enhanced fname 'arial'  fsize 10 butt solid
set output 'out.svg'

set termoption dash

filename='data.txt'

set key inside top left
set ylabel 'Deviation (%)'
set xlabel 'Time (s)'

set xrange [1:10800]
set yrange [0:50]

set logscale x

plot  \
    filename using 1:2 title 'R^{q} scale 1' with lines lt 6
