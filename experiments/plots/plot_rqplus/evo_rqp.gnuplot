# use gnuplot -e "filename='file.txt'"

# Scale font and line width (dpi) by changing the size! It will always display stretched.
set terminal svg size 400,300 enhanced fname 'arial'  fsize 10 butt solid
set output filename . '.svg'

set termoption dash

# Key means label...
set key inside bottom right
# set key off

# set xlabel 'Time (s)'
# set ylabel 'Gain (%)'

set xrange [0:10800]
set yrange [-140:80]

plot 0 notitle with lines lt -1, \
	filename using 2:4 title 'LS-s1-w60' with lines lt 7, \
	filename using 5:7 title 'LS-s1-w10' with lines lt 6, \
   	filename using 8:10 title 'LS-s2-w60' with lines lt 5,   \
    filename using 11:13 title 'LS-s2-w10' with lines lt 4,   \
    filename using 14:16 title 'LS-s*-w10' with lines lt 3,  \
    filename using 17:19 title 'LS-s1-w*' with lines lt 2,  \
    filename using 20:22 title 'LS-s*-w*' with lines lt 1

