# Scale font and line width (dpi) by changing the size! It will always display stretched.
set terminal svg size 400,300 enhanced fname 'arial'  fsize 10 butt solid
set output 'out.svg'
set termoption dash

# Key means label...
set key inside bottom right

set ylabel 'Gain (%): current sol. eval.'
set xlabel 'Time (s)'

#set xrange [0:10800]
#set yrange [0:100]

set xrange [0:10800]
set yrange [-140:80]

f(x) = (40.0 - x)/40.0 * 100

plot 0 notitle with lines lt -1, \
	"data.txt" using 4:(f($5)) title 'LS-s1-w10' with lines lt 6, \
    "data.txt" using 10:(f($11)) title 'LS-s*-w10' with lines lt 3,  \
    "data.txt" using 12:(f($13)) title 'LS-s1-w*' with lines lt 2,  \
    "data.txt" using 14:(f($15)) title 'LS-s*-w*' with lines lt 1#,  \
#    "data.txt" using 2:1 title 'w   s' with lines lt -1
