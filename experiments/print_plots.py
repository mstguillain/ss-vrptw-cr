import argparse
import json


###### ARGUMENTS ############
parser = argparse.ArgumentParser()

parser.add_argument("--solutions", required=True, help="JSON file containing the solutions", type=str)

args = parser.parse_args()
##############################

f = open(args.solutions)

j = json.load(f)

for j_sol in j["Solutions"]:
	if ("CostR+S1" in j_sol) :
		costRplusS1 = j_sol["CostR+S1"]
	else : 
		costRplusS1 = 0.0
	print(str(j_sol["ElapsedComputationTime"]) + "\t " + str(j_sol["Cost"]) + "\tScale=" + str(j_sol["Scale"]) + " W=" + str(j_sol["WaitingMultiple"]) + "\t" + str(costRplusS1))