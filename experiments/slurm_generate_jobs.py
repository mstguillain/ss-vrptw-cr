#!/usr/bin/python
import datetime, os, sys
import subprocess, time
import argparse

path_to_vrp = os.environ['HOME']+'/vrp'
travel_times = path_to_vrp+'/benchmarks/ss-vrptw-cr/realdata_optimod_lyon/travel_times.txt'
instances_base_dir = path_to_vrp+'/benchmarks/ss-vrptw-cr/realdata_optimod_lyon/instances_2/large'

code_algo = {	'LS-s2-60':'s6', 	\
				'LS-s2-30':'s3', 	\
				'LS-s2-10':'s1', 	\
				'LS-s1-60':'u6', 	\
				'LS-s1-30':'u3', 	\
				'LS-s1-10':'u1', 	\
				'LS-succ-s':'s', 	\
				'LS-succ-w':'w', 	\
				'LS-succ-ws':'ws'	}

code_instance_type = {'30w50c':'A', '50w50c':'B', '50cw':'C'}






###### ARGUMENTS ############
parser = argparse.ArgumentParser()

parser.add_argument("--range", required=True, help="Range for experiences; e.g. 1,15", type=str)
parser.add_argument("--avg", required=True, help="Number of runs of each experience", type=int)
parser.add_argument("--veh", required=True, help="Add # vehicles listed (csv); e.g. 5,10,20", type=str, default="")
parser.add_argument("--inst", required=True, help="Add instance type listed (csv); e.g. 30w50c,50w50c,50cw", type=str, default="")
parser.add_argument("--algo", required=True, help="Add algorithms listed (csv); e.g. LS_succ_s,LS_succ_w,...", type=str, default="")
parser.add_argument("--out", required=True, help="Directory where to create the result files", type=str)

parser.add_argument("--prefix", help="Add a prefix to the expe name and output file", type=str, default="")
parser.add_argument("--path_LS_bin", help="Path to the LS bin directory", type=str, default=path_to_vrp+'/ss-vrptw-cr/ls/bin')
parser.add_argument("--srun", help="Execute srun commands (default:False)", action='store_true', default=False)

args = parser.parse_args()
##############################

instance_type = args.inst.split(',')
nveh = args.veh.split(',')
nveh = map(int, nveh)
algos = args.algo.split(',')

range_expe = range(int(args.range.split(',')[0]),int(args.range.split(',')[1])+1)

path_to_results = "/" + args.out.strip("/")
if not os.path.exists(path_to_results):
	print "Error: Results directory "+path_to_results+ " does not exist."
	exit(1)

args.path_LS_bin = "/" + args.path_LS_bin.strip("/")
if not os.path.exists(args.path_LS_bin):
	print "Error: LS bin directory "+args.path_LS_bin+ " does not exist."
	exit(1)


if args.srun :
	# if not os.path.exists(path_to_results):
	# 	print 'Creating directory ' + path_to_results
	# 	os.makedirs(path_to_results)
	# else :
	# 	print 'Overwriting in directory ' + path_to_results
	print 'Commiting jobs: '

n_commands = 0
for ins_t in instance_type:
	if not args.srun: print 'class '+ins_t+':'
	for n in nveh:
		if not args.srun: print '\t#veh: '+str(n)
		for algo in algos:
			if not args.srun: print '\t\tALGO '+algo+':'
			for i in range_expe:
				if ins_t == '50cw': 
						instance = instances_base_dir+'/'+ins_t+'/50cw_'+str(i)+'.txt'
				else : 
					instance = instances_base_dir+'/'+ins_t+'/50c_'+str(i)+'.txt'

				if algo == 'LS-s1-60' :
					command = 'ssvrptwcr_LS '+str(args.avg)+' 1 '+travel_times+' '+instance+' R_CAPA '+str(n)+' 20 60 43200 1'
				if algo == 'LS-s1-30' :
					command = 'ssvrptwcr_LS '+str(args.avg)+' 1 '+travel_times+' '+instance+' R_CAPA '+str(n)+' 20 30 43200 1'
				if algo == 'LS-s1-10' :
					command = 'ssvrptwcr_LS '+str(args.avg)+' 1 '+travel_times+' '+instance+' R_CAPA '+str(n)+' 20 10 43200 1'

				if algo == 'LS-s2-60' :
					command = 'ssvrptwcr_LS '+str(args.avg)+' 1 '+travel_times+' '+instance+' R_CAPA '+str(n)+' 20 30 43200 2'
				if algo == 'LS-s2-30' :
					command = 'ssvrptwcr_LS '+str(args.avg)+' 1 '+travel_times+' '+instance+' R_CAPA '+str(n)+' 20 15 43200 2'
				if algo == 'LS-s2-10' :
					command = 'ssvrptwcr_LS '+str(args.avg)+' 1 '+travel_times+' '+instance+' R_CAPA '+str(n)+' 20 5 43200 2'

				if algo == 'LS-succ-s' :
					command = 'ssvrptwcr_LS_succ_scaling_Rq s '+travel_times+' '+instance+' '+str(n)+' 20 10 43200 '+str(args.avg)
				if algo == 'LS-succ-w' :
					command = 'ssvrptwcr_LS_succ_scaling_Rq w '+travel_times+' '+instance+' '+str(n)+' 20 10 43200 '+str(args.avg)
				if algo == 'LS-succ-ws' :
					command = 'ssvrptwcr_LS_succ_scaling_Rq ws '+travel_times+' '+instance+' '+str(n)+' 20 10 43200 '+str(args.avg)
				
				n_commands += 1
				code_expe = args.prefix+code_instance_type[ins_t] + "%02d"%n + code_algo[algo] + "-%02d"%i 
				srun_command = 'srun --job-name '+code_expe+' --output '+path_to_results+'/'+code_expe+'.txt --time=3-00:00:00 --ntasks=1 --mem-per-cpu=2000 '
				if args.srun :
					# process = subprocess.Popen(srun_command, shell=True, stdout=subprocess.PIPE)
					# process.wait()
					os.system(srun_command + ' ' + args.path_LS_bin+'/'+command + ' &')
					time.sleep(0.1)
					# print '.';
				else :
					print '\t\t' +code_expe+ ' ~ ' +srun_command
					print '\t\t'+args.path_LS_bin+'/'+command
print ''
if args.srun :
	print ' Done'
	print 'Total: ' + str(n_commands) + ' jobs commited, range: ' + str(range_expe)
else :
	print 'range: ' + str(range_expe)
	print 'total: ' + str(n_commands) 
