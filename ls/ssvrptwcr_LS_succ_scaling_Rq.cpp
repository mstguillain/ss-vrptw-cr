/*
Copyright 2017 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/



/*

The Local Search algorithm described in paper:
"The Static and Stochastic VRPTW with both random Customers and Reveal Times: algorithms and recourse strategies", Saint-Guillain et al, 2017

Handles on instance files of the type described in the same paper. 
*/



#include "VRP_instance.h"
#include "VRP_solution.h"
#include "VRP_scenario.h"
#include "NM.h"
#include "LS_Program.h"
#include "tools.h"
#include <queue>
#include <cmath>
#include <ctime>	// clock()
#include <chrono>
#include <getopt.h>

#include "json.hpp"
using json = nlohmann::json;

using namespace std;

bool request_lt(const VRP_request& r1, const VRP_request& r2) {
	if (r1 == r2) return false;

	if (r1.revealTime < r2.revealTime)							// reveal time first
		return true;
	if (r1.revealTime == r2.revealTime) {
		if (r1.l < r2.l)										// end of time window second)
			return true;
		if (r1.l == r2.l) {
			ASSERT(r1.getVertex().getRegion() != r2.getVertex().getRegion(), "no tie breaking !");
			return r1.getVertex().getRegion() < r2.getVertex().getRegion();		// region number to break ties
		}
	}

	return false;
}

// int curr_scale, curr_wait_mult;
clock_t start_time;
json j_best_solutions;
CallbackReturnType newBestSolution(Solution_SS_VRPTW_CR& bestSolution, double& eval) {
	UNUSED(eval);

	json j_sol = bestSolution.json_desc();
	double elapsed_time = (double)(clock() - start_time) / CLOCKS_PER_SEC;
	j_sol["ElapsedComputationTime"] = elapsed_time;
	j_best_solutions.push_back(j_sol);

	return NONE;
}



void help() {
	cout << "Usage: ssvrptwcr_LS [--help] " << endl;
	cout << endl 
		<< "Options:" << endl 
		<< "\t--verbose(+/++) : turn output verbose" << endl
		<< "\t--help (-h) : print this help" << endl
		<< "\t--algo (-A) N : algorithm to be used: {w,s,ws} (default: ws)" << endl
		<< "\t--number-runs (-n) N : set the number of runs to be performed before printing avg results (default: 1)" << endl
		<< "\t--instance-file (-I) FILE: only for type STGUILLAIN: the main instance file" << endl
		<< "\t--graph-file (-g) FILE: for both STGUILLAIN and CANDAELE: the file that contains travel times (and other vehicle related stuffs for CANDAELE)" << endl
		// << "\t--carrier-file (-c) FILE: only for type CANDAELE: the carrier instance file" << endl
		// << "\t--customer-file (-C) FILE: only for type CANDAELE: the customer instance file" << endl
		<< "\t--number-vehicles (-V) N: number of vehicles to be used" << endl
		<< "\t--vehicles-capacity (-Q) N: set the capacity of the vehicle(s)" << endl
		<< "\t--wait-multiple (-W) N: set the waiting time multiple to be used during optimization" << endl
		// << "\t--scale (-s) N: set the scale to be used during optimization (default: 1.0)" << endl
		<< "\t--time-limit (-T) N: set the time limit of optimization (seconds)" << endl
		<< "\t--output-json (-o) FILE_PREFIX: ouput a summary of the optimisation evolution to json files (one per run)" << endl
		<< endl;
}

int main(int argc, char **argv) {
	VRP_request::request_comparison_lt = &request_lt;
	srand(time(NULL));

	if (argc < 2) { help(); return 1; }
	
	string algo = "ws";
	const char* instance_file = nullptr;
	// const char* carrier_file = nullptr;
	// const char* customers_file = nullptr;
	const char* graph_file = nullptr;
	string instance_type;
	int verbose = 0;
	int nb_runs = 1;
	int number_vehicles = -1;
	int vehicle_capacity = -1;
	int waiting_time_multiple = -1;
	int time_limit = -1;

	string json_ouput_filename = "";

	static struct option long_options[] = {
		{"verbose"		, no_argument		, &verbose,	1},
		{"verbose+"		, no_argument		, &verbose,	2},
		{"verbose++"	, no_argument		, &verbose,	3},
		{"help"			, no_argument		, 0, 'h'},
		{"algo"			, required_argument	, 0, 'A'},
		{"number-runs"	, required_argument	, 0, 'n'},
		{"instance-file", required_argument	, 0, 'I'},
		// {"carrier-file"	, required_argument	, 0, 'c'},
		// {"customer-file", required_argument	, 0, 'C'},
		{"graph-file"	, required_argument	, 0, 'g'},
		{"number-vehicles"	, required_argument	, 0, 'V'},
		{"vehicle-capacity"	, required_argument	, 0, 'Q'},
		{"wait-multiple"	, required_argument	, 0, 'W'},
		{"time-limit"	, required_argument	, 0, 'T'},
		// {"scale"		, required_argument	, 0, 's'},
		{"output-json"		, required_argument	, 0, 'o'},
		{0, 0, 0, 0}
	};
	int c, option_index = 0;
	while ((c = getopt_long (argc, argv, "hA:n:t:I:g:V:Q:W:T:s:o:", long_options, &option_index)) != -1 ) {
		vector<string> x;
		switch (c) {
			case 'h': help(); return 1;
			case 'n': nb_runs = atoi(optarg); break;
			case 'A': algo = optarg; break;
			case 'I': instance_file = optarg; break;
			// case 'c': carrier_file = optarg; break;
			// case 'C': customers_file = optarg; break;
			case 'g': graph_file = optarg; break;
			case 'V': number_vehicles = atoi(optarg); break;
			case 'Q': vehicle_capacity = atoi(optarg); break;
			case 'W': waiting_time_multiple = atoi(optarg); break;
			case 'T': time_limit = atoi(optarg); break;
			// case 's': scale = atoi(optarg); break;
			case 'o': json_ouput_filename = optarg; break;
			case '?': abort();
		}
	}

	if (instance_file == nullptr) cout << "ERROR: the instance file must be specified with --instance-file (-I) FILE" << endl, abort();
	if (graph_file == nullptr) cout << "ERROR: the travel times instance file must be specified with --graph-file (-g) FILE" << endl, abort();

	if (vehicle_capacity < 0) cout << "ERROR: required argument: --vehicle-capacity (-Q) N" << endl, abort();	
	if (number_vehicles < 0) cout << "ERROR: required argument: --number-vehicles (-n) N" << endl, abort();

	if (waiting_time_multiple < 0) cout << "ERROR: Required argument: --wait-multiple (-W) N" << endl, abort();
	if (time_limit < 0) cout << "ERROR: Required argument: --time-limit (-T) N" << endl, abort();
	

	const char * travel_times_file = graph_file;

	// CREATE SCALED INSTANCES
	VRP_instance & I_scale1 = VRP_instance_file::readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs(travel_times_file, instance_file, vehicle_capacity, 1);
	VRP_instance & I_scale2 = VRP_instance_file::readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs(travel_times_file, instance_file, vehicle_capacity, 2);
	VRP_instance & I_scale5 = VRP_instance_file::readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs(travel_times_file, instance_file, vehicle_capacity, 5);

	cout << endl;
	cout << "Instance file: " << instance_file << endl;
	cout << I_scale1.toString() << endl;
	cout << "Wait. time multiple:\t" << setw(3) << waiting_time_multiple << endl << endl;

	vector<int> wait_time_multiple_factors;
	bool succ_scale = false;
	double computation_time;
	if (algo == "s") {
		cout << "SUCESSIVE SCALING (Rq only, s5 -> s2 -> s1)" << endl;
		wait_time_multiple_factors = {1};
		succ_scale = true;
		computation_time = time_limit / (double) 3;
	}
	else if (algo == "w") {
		cout << "SUCESSIVE WTime Multiples (Rq only, scale 1, wmult: x6 -> x3 -> x1)" << endl;
		wait_time_multiple_factors = {6, 3, 1};
		computation_time = time_limit / (double) 3;
	}
	else if (algo == "ws") {
		cout << "SUCESSIVE WTime Multiples / SCALING (Rq only, wmult: x6 with {s5, s2, s1} -> x3 with {s5, s2, s1} -> x1 with {s5, s2, s1})" << endl;
		wait_time_multiple_factors = {6, 3, 1};
		succ_scale = true;
		computation_time = time_limit / (double) 9;
	} else _ASSERT_(false, "wrong algo: " << algo);


	double avg_cost = 0.0;
	for (int run_ = 1; run_ <= nb_runs; run_++) {
		j_best_solutions.clear();
		if (nb_runs > 1)
			cout << outputMisc::redExpr(true) << "##########" << endl << "# RUN " << setw(2) << run_ << " #" << endl << "##########" <<  outputMisc::resetColor() << endl;

		int initialWaitingTime = 60;	// one hour initial waiting time, always

		// CREATE SOLUTIONS
		Solution_SS_VRPTW_CR sol__Rq_s1(I_scale1, R_CAPA);
		Solution_SS_VRPTW_CR sol__Rq_s2(I_scale2, R_CAPA);
		Solution_SS_VRPTW_CR sol__Rq_s5(I_scale5, R_CAPA);
		sol__Rq_s1.setMinimumWaitingTime(1.0);
		sol__Rq_s1.setInitialWaitingTime(initialWaitingTime);
		sol__Rq_s1.setWaitingTimeMultiple(6 * waiting_time_multiple);
		sol__Rq_s2.setMinimumWaitingTime(1.0);
		sol__Rq_s2.setInitialWaitingTime(initialWaitingTime/2);
		sol__Rq_s2.setWaitingTimeMultiple(6 * waiting_time_multiple / 2);
		sol__Rq_s5.setMinimumWaitingTime(1.0);
		sol__Rq_s5.setInitialWaitingTime(initialWaitingTime/5);
		sol__Rq_s5.setWaitingTimeMultiple(6 * waiting_time_multiple / 5);
		sol__Rq_s1.addRoutes(number_vehicles);
		sol__Rq_s2.addRoutes(number_vehicles);
		sol__Rq_s5.addRoutes(number_vehicles);
		sol__Rq_s1.generateInitialSolution(true);
		sol__Rq_s2.generateInitialSolution(true);
		sol__Rq_s5.generateInitialSolution(true);

		enum SeqElem {
			RQ_S5,		
			RQ_S2,		
			RQ_S1,		
		};
		const char* desc_seq[] = {"Rq_s5", "Rq_s2", "Rq_s1"};
		Solution_SS_VRPTW_CR * sol_seq [] = {&sol__Rq_s5, &sol__Rq_s2, &sol__Rq_s1};
	
		start_time = clock();	
	
		int prev_algo = -1;
		Solution_SS_VRPTW_CR *current_sol;
		for (int i : wait_time_multiple_factors) {


			// CREATE NEIGHBORHOOD MANAGERS
			NeighborhoodManager<Solution_SS_VRPTW_CR> nm__Rq_scale1(sol__Rq_s1, SS_VRPTW_CR, I_scale1.getHorizon(), i * waiting_time_multiple, ceil(I_scale1.getHorizon() / 4));
			NeighborhoodManager<Solution_SS_VRPTW_CR> nm__Rq_scale2(sol__Rq_s2, SS_VRPTW_CR, I_scale2.getHorizon(), i * waiting_time_multiple/2, ceil(I_scale2.getHorizon() / 4));
			NeighborhoodManager<Solution_SS_VRPTW_CR> nm__Rq_scale5(sol__Rq_s5, SS_VRPTW_CR, I_scale5.getHorizon(), i * waiting_time_multiple/5, ceil(I_scale5.getHorizon() / 4));

			NeighborhoodManager<Solution_SS_VRPTW_CR> * nm_seq [] = {&nm__Rq_scale5, &nm__Rq_scale2, &nm__Rq_scale1};

			int curr_algo;
			if (succ_scale) curr_algo = RQ_S5;
			else curr_algo = RQ_S1;
			for (; curr_algo <= RQ_S1; curr_algo++) {
				if (prev_algo != -1 && succ_scale) {
					cout << "copying solution from " << desc_seq[prev_algo] << " into " << desc_seq[curr_algo] << endl;
					sol_seq[curr_algo]->copyFromSol_scaleWaitingTimes(*sol_seq[prev_algo], true);
				}

				current_sol = sol_seq[curr_algo];
				NeighborhoodManager<Solution_SS_VRPTW_CR> *current_nm = nm_seq[curr_algo];
				const char * current_desc = desc_seq[curr_algo];

				
				int wmult = (waiting_time_multiple / current_sol->getScale()) * i ;
				current_sol->setWaitingTimeMultiple(wmult);
				cout << endl << "Optimizing " << current_desc << " [wmult=" << wmult << "] with initial solution (cost = " << current_sol->getCost() << ") for " << computation_time << "s:" << endl << current_sol->toString();
				LS_Program_SimulatedAnnealing<Solution_SS_VRPTW_CR,NeighborhoodManager<Solution_SS_VRPTW_CR>> lsprog;
				// LS_Program_Basic<Solution_SS_VRPTW_CR,NeighborhoodManager<Solution_SS_VRPTW_CR>> lsprog;
				lsprog.setTimeOut(computation_time);
				if (json_ouput_filename != "") {
					double x = 0.0;	newBestSolution(*current_sol, x);
					lsprog.set_NewBestSolution_Callback(newBestSolution);
				}
				lsprog.run(*current_sol, *current_nm, numeric_limits<int>::max(), 2.0, 0.95, true);	// LS_Program_SimulatedAnnealing args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)
				// lsprog.run(*current_sol, *current_nm, numeric_limits<int>::max(), 0);	// LS_Program_Basic args: (Solution &solution, NM &nm, long long max_iter, int diversification = 10, int max_distance = 2500, bool verbose = false)
				cout << "Done ! Cost: " << current_sol->getCost() << "   Sol: " << endl << current_sol->toString() << endl << endl;	

				prev_algo = curr_algo;
			}
		}

		if (json_ouput_filename != "") {
			cout << endl << "Writting JSON summary file...";
			ofstream json_output_file;
			json j; 

			time_t t = chrono::system_clock::to_time_t(chrono::system_clock::now());
			j["CurrentDateTime"] = ctime(& t);
			j["Algo"] = "LS_succ";
			j["Scales"] = {1, 2, 5};
			j["NumberVehicles"] = number_vehicles;
			j["VehicleCapacity"] = vehicle_capacity;
			j["Solutions"] = j_best_solutions;
			json_output_file.open(json_ouput_filename + ".run-" + to_string(run_) + ".json", ofstream::out | ios::trunc); 
			json_output_file << j.dump(4);
			json_output_file.close();
			cout << "    Done." << endl;
		}
		Solution_SS_VRPTW_CR s_RCAPAPLUS(I_scale1, R_CAPA_PLUS);
		s_RCAPAPLUS.addRoutes(number_vehicles);
		s_RCAPAPLUS.generateInitialSolution(true);
		s_RCAPAPLUS.copyFromSol_scaleWaitingTimes(sol__Rq_s1);
		cout << "Run #" << run_ << " : Cost under scale 1 (R_CAPA_PLUS) ==> " << s_RCAPAPLUS.getCost() <<  endl;
		cout << "=========================================================================" << endl << endl;

		avg_cost += s_RCAPAPLUS.getCost() / nb_runs;
	}

	cout << endl;
	cout << "###########################################################################" << endl;
	cout << "Avg cost / " << nb_runs << " runs: " << avg_cost << endl;
	cout << "###########################################################################" << endl;


}



