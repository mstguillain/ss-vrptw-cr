/*
Copyright 2017 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/




#include "VRP_instance.h"
#include "VRP_solution.h"
#include "VRP_scenario.h"
#include "NM.h"
#include "LS_Program.h"
#include "tools.h"
#include <queue>
#include <cmath>
#include <getopt.h>
#include <chrono>
#include <ctime>


#include "json.hpp"
using json = nlohmann::json;

using namespace std;

bool request_lt(const VRP_request& r1, const VRP_request& r2) {
	if (r1 == r2) return false;

	if (r1.revealTime < r2.revealTime)							// reveal time first
		return true;
	if (r1.revealTime == r2.revealTime) {
		if (r1.l < r2.l)										// end of time window second)
			return true;
		if (r1.l == r2.l) {
			ASSERT(r1.getVertex().getRegion() != r2.getVertex().getRegion(), "no tie breaking !");
			return r1.getVertex().getRegion() < r2.getVertex().getRegion();		// region number to break ties
		}
	}

	return false;
}



RecourseStrategy strategy;

VRP_ScenarioPool_Volatile<Scenario_SS_DS_VRPTW_CR, Solution_SS_VRPTW_CR> *scenarioPool_big;

clock_t t;
json j_best_solutions;

CallbackReturnType newBestSolution(Solution_SS_VRPTW_CR& bestSolution, double& eval) {
	UNUSED(eval);

	json j_sol = bestSolution.json_desc();
	double elapsed_time = (double)(clock() - t) / CLOCKS_PER_SEC;
	j_sol["ElapsedComputationTime"] = elapsed_time;
	j_best_solutions.push_back(j_sol);

	return NONE;
}

void help() {
	cout << "Usage: ssvrptwcr_LS [--help] " << endl;
	cout << endl 
		<< "Options:" << endl 
		<< "\t--verbose(+/++) : turn output verbose" << endl
		<< "\t--help (-h) : print this help" << endl
		<< "\t--number-runs (-n) N : set the number of runs to be performed before printing avg results (default: 1)" << endl
		<< "\t--type-instance (-t) TYPE: specifies the type of instance files provided; either STGUILLAIN or CANDAELE" << endl
		<< "\t--recourse-strategy (-R) RECOURSE: set the recourse strategy to be used; {R_INFTY,R_CAPA,R_CAPA_PLUS}" << endl
		<< "\t--instance-file (-I) FILE: only for type STGUILLAIN: the main instance file" << endl
		<< "\t--graph-file (-g) FILE: for both STGUILLAIN and CANDAELE: the file that contains travel times (and other vehicle related stuffs for CANDAELE)" << endl
		<< "\t--carrier-file (-c) FILE: only for type CANDAELE: the carrier instance file" << endl
		<< "\t--customer-file (-C) FILE: only for type CANDAELE: the customer instance file" << endl
		<< "\t--number-vehicles (-V) N: number of vehicles to be used" << endl
		<< "\t--vehicles-capacity (-Q) N: set the capacity of the vehicle(s)" << endl
		<< "\t--wait-multiple (-W) N: set the waiting time multiple to be used during optimization" << endl
		<< "\t--scale (-s) N: set the scale to be used during optimization (default: 1.0)" << endl
		<< "\t--time-limit (-T) N: set the time limit of optimization (seconds)" << endl
		<< "\t--output-json (-o) FILE_PREFIX: ouput a summary of the optimisation evolution to json files (one per run)" << endl
		<< endl;
}

int main(int argc, char **argv) {
	if (argc < 2) { help(); return 1; }

	VRP_request::request_comparison_lt = &request_lt;
	srand(time(NULL));

	const char* instance_file = nullptr;
	const char* carrier_file = nullptr;
	const char* customers_file = nullptr;
	const char* graph_file = nullptr;
	string instance_type;
	int verbose = 0;
	int nb_runs = 1;
	string recourse_strategy_str;
	int number_vehicles = -1;
	int vehicle_capacity = -1;
	int waiting_time_multiple = -1;
	int time_limit = -1;
	int scale = 1;

	string json_ouput_filename = "";

	static struct option long_options[] = {
		{"verbose"		, no_argument		, &verbose,	1},
		{"verbose+"		, no_argument		, &verbose,	2},
		{"verbose++"	, no_argument		, &verbose,	3},
		{"help"			, no_argument		, 0, 'h'},
		{"number-runs"	, required_argument	, 0, 'n'},
		{"type-instance", required_argument	, 0, 't'},
		{"recourse-strategy", required_argument	, 0, 'R'},
		{"instance-file", required_argument	, 0, 'I'},
		{"carrier-file"	, required_argument	, 0, 'c'},
		{"customer-file", required_argument	, 0, 'C'},
		{"graph-file"	, required_argument	, 0, 'g'},
		{"number-vehicles"	, required_argument	, 0, 'V'},
		{"vehicle-capacity"	, required_argument	, 0, 'Q'},
		{"wait-multiple"	, required_argument	, 0, 'W'},
		{"time-limit"	, required_argument	, 0, 'T'},
		{"scale"		, required_argument	, 0, 's'},
		{"output-json"		, required_argument	, 0, 'o'},
		{0, 0, 0, 0}
	};
	int c, option_index = 0;
	while ((c = getopt_long (argc, argv, "hn:t:R:I:c:C:g:V:Q:W:T:s:o:", long_options, &option_index)) != -1 ) {
		vector<string> x;
		switch (c) {
			case 'h': help(); return 1;
			case 'n': nb_runs = atoi(optarg); break;
			case 't': instance_type.assign(optarg); break;
			case 'R': recourse_strategy_str.assign(optarg); break;
			case 'I': instance_file = optarg; break;
			case 'c': carrier_file = optarg; break;
			case 'C': customers_file = optarg; break;
			case 'g': graph_file = optarg; break;
			case 'V': number_vehicles = atoi(optarg); break;
			case 'Q': vehicle_capacity = atoi(optarg); break;
			case 'W': waiting_time_multiple = atoi(optarg); break;
			case 'T': time_limit = atoi(optarg); break;
			case 's': scale = atoi(optarg); break;
			case 'o': json_ouput_filename = optarg; break;
			case '?': abort();
		}
	}
	if (instance_type != "STGUILLAIN" && instance_type != "CANDAELE") cout << "ERROR: argument --type-instance must be set to either STGUILLAIN or CANDAELE." << endl, abort();
	if (instance_type == "STGUILLAIN") {
		if (instance_file == nullptr) cout << "ERROR: if instance type is set to STGUILLAIN, the instance file must be specified with --instance-file (-I) FILE" << endl, abort();
		if (graph_file == nullptr) cout << "ERROR: if instance type is set to STGUILLAIN, the travel times instance file must be specified with --graph-file (-g) FILE" << endl, abort();
		if (vehicle_capacity < 0) cout << "ERROR: if instance type is set to STGUILLAIN, required argument: --vehicle-capacity (-Q) N" << endl, abort();
	}
	if (instance_type == "CANDAELE") {
		if (carrier_file == nullptr) cout << "ERROR: if instance type is set to CANDAELE, the carrier instance file must be specified with --carrier-file (-c) FILE" << endl, abort();
		if (customers_file == nullptr) cout << "ERROR: if instance type is set to CANDAELE, the customer instance file must be specified with --customer-file (-C) FILE" << endl, abort();
		if (graph_file == nullptr) cout << "ERROR: if instance type is set to CANDAELE, the graph instance file must be specified with --graph-file (-g) FILE" << endl, abort();
	}
	if (number_vehicles < 0) cout << "ERROR: Required argument: --number-vehicles (-n) N" << endl, abort();
	if (waiting_time_multiple < 0) cout << "ERROR: Required argument: --wait-multiple (-W) N" << endl, abort();
	if (time_limit < 0) cout << "ERROR: Required argument: --time-limit (-T) N" << endl, abort();
	


	// RecourseStrategy strategy;
	if (recourse_strategy_str == "R_INFTY") 			strategy = R_INFTY;
	else if (recourse_strategy_str == "R_CAPA") 		strategy = R_CAPA;
	else if (recourse_strategy_str == "R_CAPA_PLUS") 	strategy = R_CAPA_PLUS;
	else ASSERT(false, "wrong recourse strategy: " << recourse_strategy_str << " (recourse strategy should be set with argument --recourse-strategy (-R) RECOURSE)");

	VRP_instance *I480;
	VRP_instance *instance;

	if (instance_type == "STGUILLAIN") {
		I480 = & VRP_instance_file::readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs(graph_file, instance_file, vehicle_capacity, 1.0);
		instance = & VRP_instance_file::readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs(graph_file, instance_file, vehicle_capacity, scale);
	}
	if (instance_type == "CANDAELE") {
		I480 = & VRP_instance_file::readInstanceFile_SSVRPTW_CR__Candaele(carrier_file, graph_file, customers_file, vehicle_capacity);
		instance = & VRP_instance_file::readInstanceFile_SSVRPTW_CR__Candaele(carrier_file, graph_file, customers_file, vehicle_capacity);
	}
	VRP_instance & I = *instance;


	Solution_SS_VRPTW_CR s480(*I480, strategy);
	s480.addRoutes(number_vehicles);
	s480.generateInitialSolution(true);
	
	cout << I.toString() << endl;
	cout << "#Vehicles: " << number_vehicles << " (Q=" << s480.getVehicleTypeAtRoute(1).getCapacity() << ")" << endl;
	cout << "RecourseStrategy : " << recourse_strategy_str << endl;
	cout << "Wait. time multiple:\t" << setw(3) << waiting_time_multiple << endl;
	// cout << I.toStringCoords(true) << endl;
	if (verbose >= 3) {
		cout << I.toStringVehicleTypes() << endl;
		cout << I.toString_TravelTimes(true) << endl;
		cout << I.toStringProbasTS() << endl << endl;
	}


	double avg_cost = 0.0;
	// double avg_cost_RCAPAPLUS = 0.0;
	for (int i = 1; i <= nb_runs; i++) {
		j_best_solutions.clear();

		if (nb_runs > 1)
			cout << outputMisc::redExpr(true) << "##########" << endl << "# RUN " << setw(2) << i << " #" << endl << "##########" <<  outputMisc::resetColor() << endl;
		// SOLUTION 
		// int x = rand() % (int) floor((I.getHorizon()/2) / waiting_time_multiple) + 1;
		// int initialWaitingTime = waiting_time_multiple * x;
		// int initialWaitingTime = waiting_time_multiple;
		int initialWaitingTime = max(waiting_time_multiple, (int)floor(60 / scale));
		// if (waiting_time_multiple == 1)
		// 	initialWaitingTime = floor(I.getHorizon() / 10);

		Solution_SS_VRPTW_CR s(I, strategy);
		s.setWaitingTimeMultiple(waiting_time_multiple);
		s.setMinimumWaitingTime(1.0);
		s.setInitialWaitingTime(initialWaitingTime);
		s.addRoutes(number_vehicles);

		// NEIGHBORHOOD MANAGER
		int minIncrement = waiting_time_multiple;
		int maxIncrement;
		if (waiting_time_multiple == 1) 
			maxIncrement = ceil(I.getHorizon() / 5);
		else 
			maxIncrement = I.getHorizon()/5;
		NeighborhoodManager<Solution_SS_VRPTW_CR> nm(s, SS_VRPTW_CR, I.getHorizon(), minIncrement, maxIncrement);
		// NeighborhoodManager<Solution_SS_VRPTW_CR> nm(s, "VRP_basic");


		// MAIN
		t = clock();
		s.generateInitialSolution(true);

		// LS_Program_Basic<Solution_SS_VRPTW_CR,NeighborhoodManager<Solution_SS_VRPTW_CR>> lsprog;
		// lsprog.run(s, nm, 10000, 10, 30, true);			// args: (Solution &solution, NM &nm, int max_iter, int diversification = 10, int max_distance = 2500, bool verbose = false)		
		LS_Program_SimulatedAnnealing<Solution_SS_VRPTW_CR,NeighborhoodManager<Solution_SS_VRPTW_CR>> lsprog;
		// lsprog.set_Time_Callback(printBestSolutionCost, 1.0);
		if (json_ouput_filename != "") {
			double x = 0.0;	newBestSolution(s, x);
			lsprog.set_NewBestSolution_Callback(newBestSolution);
		}
		lsprog.setTimeOut(time_limit);
		lsprog.run(s, nm, numeric_limits<int>::max(), 5.0, 0.995, verbose);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)



		cout << endl << "Solution at run " << i << "(scale " << s.getScale() << "):" << endl ;
		cout << s.toString(false) << s.getCost() <<  endl;

		s480.copyFromSol_scaleWaitingTimes(s);
		_ASSERT_(s480.getNumberViolations() == 0, s480.toString(true));
		cout << "Cost under scale 1 (" << recourse_strategy_str << ")" << endl << s480.toString(false) << s480.getCost() <<  endl;


		// VRP_ScenarioPool_Volatile<Scenario_SS_VRPTW_CR, Solution_SS_VRPTW_CR> scenarioPool_big(*I480, 1000000);
		// cout << "experimental expected cost = \t" << scenarioPool_big.computeExperimentalExpectedCost(*s480, strategy) << endl;


		// Solution_SS_VRPTW_CR s_RCAPAPLUS(*I480, R_CAPA_PLUS);
		// s_RCAPAPLUS.addRoutes(number_vehicles);
		// s_RCAPAPLUS.generateInitialSolution(true);
		// s_RCAPAPLUS.copyFromSol_scaleWaitingTimes(s480);
		// cout << "Cost under scale 1 (R_CAPA_PLUS) ==> " << endl << s_RCAPAPLUS.toString() << s_RCAPAPLUS.getCost() <<  endl;

		avg_cost += s480.getCost() / nb_runs;
		// avg_cost_RCAPAPLUS += s_RCAPAPLUS.getCost() / nb_runs;

		if (json_ouput_filename != "") {
			cout << endl << "Writting JSON summary file...";
			ofstream json_output_file;
			json j; 

			time_t t = chrono::system_clock::to_time_t(chrono::system_clock::now());
			j["CurrentDateTime"] = ctime(& t);
			j["Algo"] = "LS";
			j["NumberVehicles"] = number_vehicles;
			j["VehicleCapacity"] = vehicle_capacity;
			j["RecourseStrategy"] = recourse_strategy_str;
			j["WaitingTimeMultiple"] = waiting_time_multiple;
			j["Scale"] = scale;
			j["Solutions"] = j_best_solutions;
			json_output_file.open(json_ouput_filename + ".run-" + to_string(i) + ".json", ofstream::out | ios::trunc); 
			json_output_file << j.dump(4);
			json_output_file.close();
			cout << "    Done." << endl;
		}
		cout << "=========================================================================" << endl << endl;
	}
	cout << endl;
	cout << "###########################################################################" << endl;
	cout << "Avg cost / " << nb_runs << " runs: " << avg_cost << endl;
	// cout << "RCAPAPLUS : Avg cost / " << nb_runs << " runs: " << avg_cost_RCAPAPLUS << endl;
	cout << "###########################################################################" << endl;
	// cout << s.toString(true) << endl;

}



