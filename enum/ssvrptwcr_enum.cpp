#include "VRP_instance.h"
#include "VRP_solution.h"
#include "VRP_scenario.h"
#include "NM.h"
#include "LS_Program.h"
#include "tools.h"
#include <queue>
#include <cmath>
#include <limits>
#include <vector>
#include <set>
#include <algorithm> // next_permutation, sort
#include <ctime>	// clock()

using namespace std;

int n_sol = 0;

bool request_lt(const VRP_request& r1, const VRP_request& r2) {
	if (r1 == r2) return false;

	if (r1.revealTime < r2.revealTime)							// reveal time first
		return true;
	if (r1.revealTime == r2.revealTime) {
		if (r1.l < r2.l)										// end of time window second)
			return true;
		if (r1.l == r2.l) {
			ASSERT(r1.getVertex().getRegion() != r2.getVertex().getRegion(), "no tie breaking !");
			return r1.getVertex().getRegion() < r2.getVertex().getRegion();		// region number to break ties
		}
	}

	return false;
}


/* generate all the subsets of fixed size subset_size from the set {1,...,n} to the vector subsets 
	using efficient method described here: http://graphics.stanford.edu/~seander/bithacks.html#NextBitPermutation
	encoding: bitset 	*/
void generate_subsets(int n, int subset_size, vector<unsigned int> & subsets) {
	const int max_bits = sizeof(unsigned int) * 8;
	_ASSERT_(n <= max_bits, "");
	unsigned int v = (1 << subset_size) - 1; // current permutation of bits 
	unsigned int w = 0; // next permutation of bits
	unsigned int max = (1 << n) - (1 << (n-subset_size));
	// cout << bitset<32>(v) << endl;
	// cout << bitset<32>(max) << endl;
	while (v != max) {
		subsets.push_back(v);
		
		unsigned int t = v | (v - 1); // t gets v's least significant 0 bits set to 1
		// Next set to 1 the most significant bit to change, 
		// set to 0 the least significant ones, and add the necessary 1 bits.
		w = (t + 1) | (((~t & -~t) - 1) >> (__builtin_ctz(v) + 1));  	
		
		v = w;
	}
	subsets.push_back(v);
}



/* a vrp permutation represents a set of n_vehicles routes, each seperated by a 0 vertex
	from a bitset of vertices, generate all the possible vrp permutations	*/
void generate_permuts_vrp(int n_vehicles, const unsigned int bitset, vector<vector<int>> & permutations) {
	// cout << "Bitset: " << bitset32_toString(bitset) << endl;
	vector<int> permut;

	for (int i=0; i < n_vehicles-1; i++)	
			permut.push_back(0);			// add a 0 vertex for each additional depot to the tsp
	for (int i = 0; i < 32; i++)
		if ((1 << i) & bitset) 
			permut.push_back(i+1);			// then add each vertex of the bitset
	

	// sort(vertices.begin(), vertices.end());	==> is always sorted by construction from the bitset, don't need this line ! 
	do {
		bool ok = true;
		if (permut[0] == 0 || permut[permut.size()-1] == 0)
			ok = false;
		if (n_vehicles > 2)
			for (int i = 1; i < (int)permut.size() && ok; i++)
				if (permut[i-1] == permut[i])
					ok = false;
		if (ok) 
			permutations.push_back(permut);
	} while(next_permutation(permut.begin(), permut.end()));
}
void generate_permuts_tsp(const unsigned int bitset, vector<vector<int>> & permutations) {
	generate_permuts_vrp(1, bitset, permutations);
}

int number_of_bits1(uint32_t i) {
	i = i - ((i >> 1) & 0x55555555);
	i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
	return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
}

string& bitset32_toString(unsigned int s) {
	ostringstream out; 
	// out << bitset<32>(s) << ":  ";
	for (int i = 0; i < 32; i++)
		if ((1 << i) & s) 
			out << i+1 << "  ";
	static string str = ""; str = out.str();
	return (str);
}

void generate_vrp_solutions_2veh(const vector<unsigned int> bitsets, vector<vector<int>> & vrp_solutions) {
	vector<vector<unsigned int>> veh_assignments;
	for (int i = 0; i < (int)bitsets.size(); i++) {
		uint32_t s1 = bitsets[i];
		for (int j = i+1; j < (int)bitsets.size(); j++) {
			uint32_t s2 = bitsets[j];
			// cout << "testing   " <<  bitset32_toString(s1) << " \t and " << bitset32_toString(s2) << endl;
			if ((s1 & s2) == 0) {
				vector<unsigned int> bitset_couple;
				bitset_couple.push_back(s1);
				bitset_couple.push_back(s2);
				veh_assignments.push_back(bitset_couple);
			}
		}
	}


	for (vector<unsigned int> bitset_couple : veh_assignments) {		// for each possible set of vehicle assigments
		// cout << "VRP solutions given assigments: " << bitset32_toString(bitset_couple[0]) << ",  " << bitset32_toString(bitset_couple[1]) << endl;
		vector<vector<int>> permutations_1;							// for vehicle 1 assigment 
		generate_permuts_tsp(bitset_couple[0], permutations_1);				// generate all the possible TSPs
		vector<vector<int>> permutations_2;							// for vehicle 2 assigment 
		generate_permuts_tsp(bitset_couple[1], permutations_2);				// generate all the possible TSPs

		for (int i = 0; i < (int)permutations_1.size(); i++) {			// then, add each couple of TSPs as being a VRP solution 
			vector<int> & permut_1 = permutations_1[i];
			for (int j = 0; j < (int)permutations_2.size(); j++) {
				vector<int> & permut_2 = permutations_2[j];

				vector<int> vrp_sol = permut_1;
				vrp_sol.push_back(0);
				vrp_sol.insert(vrp_sol.end(), permut_2.begin(), permut_2.end());

				vrp_solutions.push_back(vrp_sol);
				// cout << "\t";
				// for (int v : vrp_sol)
				// 	cout << v << "  ";
				// cout << endl;
			}
		}
	}
}


void additional_waitings(int n_vertices, int remaining_time, vector<int> curr, vector<vector<int>> & blah) {
	if (n_vertices == 1) {
		curr.push_back(remaining_time);
		blah.push_back(curr);
	} else {
		for (int i = 0; i <= remaining_time; i++) {
			vector<int> curr_ = curr;
			curr_.push_back(i);
			additional_waitings(n_vertices-1, remaining_time-i, curr_, blah);
		}
	}
}

int main(int argc, char **argv) {
	if (argc < 9) {
		cout << "Usage: ssvrptw_enum travel_times_file instance_file recourse_strategy number_vehicles vehicle_capacity waiting_time_multiple time_limit scale" << endl;
		return 1;
	}
	const char * travel_times_file = argv[1];
	const char * instance_file = argv[2];
	const string recourse_strategy_str(argv[3]);
	const int number_vehicles = atoi(argv[4]);
	const int vehicle_capacity = atoi(argv[5]);
	const int waiting_time_multiple = atoi(argv[6]);
	const int time_limit = atoi(argv[7]);
	const int scale = atoi(argv[8]);

	RecourseStrategy strategy = R_CAPA;
	if (recourse_strategy_str == "R_INFTY") 			strategy = R_INFTY;
	else if (recourse_strategy_str == "R_CAPA") 		strategy = R_CAPA;
	else if (recourse_strategy_str == "R_CAPA_PLUS") 	strategy = R_CAPA_PLUS;
	else ASSERT(false, "wrong recourse strategy: " << recourse_strategy_str);



	// cout << "Instance file: " << instance_file << " \t Timeout: " << timeout << "s" << " \t scale: " << scale << endl;
	VRP_instance & I = VRP_instance_file::readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs(travel_times_file, instance_file, vehicle_capacity, scale);
	int NVertices = I.getNumberVertices(VRP_vertex::WAITING);
	int NVeh = 2;
	int H = I.getHorizon();
	_ASSERT_(NVertices <= 32, "");
	cout << I.toString() << endl;

	clock_t	start_clock = clock();
	double elapsed_time = 0.0, last_elapsed_time_print = 0.0;


	// We only consider solutions in which all the vehicles are used !!
	// First, generate the subsets having the appropriate number of elements (between 1 and NVertices-n_vehicles+1)
	cout << "Generating subsets ..." << endl << flush;
	vector<unsigned int> subsets;
	for (int subset_size = 1; subset_size <= NVertices-number_vehicles+1; subset_size++)
		generate_subsets(NVertices, subset_size, subsets);
	// cout << "Subsets:" << endl;
	// for (unsigned int subset : subsets)
	// 	cout << bitset32_toString(subset) << endl;

	elapsed_time = (double) (clock() - start_clock) / CLOCKS_PER_SEC;
	if (elapsed_time >= time_limit) {
		cout << "Timeout ! Abort" << endl;
		return 0;
	}
		

	cout << "Generating VRP solutions ..." << endl << flush;
	vector<vector<int>> vrp_solutions;
	switch(number_vehicles) {
		case 2: generate_vrp_solutions_2veh(subsets, vrp_solutions); break;
		default: _ASSERT_(false, "");
	}

	elapsed_time = (double) (clock() - start_clock) / CLOCKS_PER_SEC;
	if (elapsed_time >= time_limit) {
		cout << "Timeout ! Abort" << endl;
		return 0;
	}

	VRP_request::request_comparison_lt = &request_lt;
	int initial_waiting_time = waiting_time_multiple;
	Solution_SS_VRPTW_CR s(I, strategy);
	s.addRoutes(number_vehicles);
	s.generateInitialSolution(true);
	Solution_SS_VRPTW_CR best_sol = s;

	double ***vars_x;
	vars_x = new double**[NVertices+1];
	for (int i = 0; i < NVertices+1; i++) {
		vars_x[i] = new double*[NVertices+1];
		for (int j = 0; j < NVertices+1; j++)
			vars_x[i][j] = new double[NVeh+1];
	}
	double ***vars_tau;
	vars_tau = new double**[NVertices+1];
	for (int i = 0; i < NVertices+1; i++) {
		vars_tau[i] = new double*[H+1];
		for (int l = 0; l < H+1; l++) 
			vars_tau[i][l] = new double[NVeh+1];
	}

	cout << "Iterating over each VRP solution (enumerate all possible waiting time configurations) ..." << endl << flush;
	cout << fixed << "\tCPU Time           Best          #sol " << endl << flush;
	for (vector<int> vrp_sol : vrp_solutions) {
		for (int i = 0; i < NVertices+1; i++)
			for (int j = 0; j < NVertices+1; j++)
				for(int p = 0; p < NVeh+1; p++)
					vars_x[i][j][p] = 0.0;
		for (int i = 0; i < NVertices+1; i++)
			for (int l = 0; l < H+1; l++)
				for(int p = 0; p < NVeh+1; p++)
					vars_tau[i][l][p] = 0.0;

		int route = 1, prev_v = 0;
		for (int v : vrp_sol) {
			vars_x[prev_v][v][route] = 1;
			if (v != 0) {
				vars_tau[v][initial_waiting_time][route] = 1;
			} else {
				route++;
			}
			prev_v = v;
		}
		vars_x[prev_v][0][route] = 1;

		for (int i = 0; i < NVertices+1; i++) 
			for (int j = 0; j < NVertices+1; j++)
				for (int p = 1; p < NVeh+1; p++)
					s.mipsol_setX(i, j, p, vars_x[i][j][p]);
		s.update_routes_from_mipsol();

		for (int i = 0; i < NVertices+1; i++) 
			for (int l = 0; l < H+1; l++)
				for (int p=1; p<NVeh+1; p++)
					s.mipsol_setTAU(i,l,p, vars_tau[i][l][p]);
		s.update_waitingTimes_from_mipsol();

		// cout << s.toString(true) << "cost: " << s.getCost() << endl;
		vector<int> v {1,0,5,3};
		// vector<int> v {0};
		if (vrp_sol == v)
			cout << s.toString() << endl;

		if (s.isFeasible()) {

			vector<vector<int>> wait_times[3];
			_ASSERT_(NVeh == 2, "");
			for (int route=1; route <= NVeh; route++) {
				int n = s.getRouteSize(route) -1;
				int d = H+1 - s.getArrivalTimeAtPosition(route, n+1);
				int additional_waiting_multiples = floor(d / waiting_time_multiple);
				// cout << "Route " << route << ": " << additional_waiting_multiples << " possible add. wait. multiples (rem. time units: " << d << ")   " << endl;
				vector<int> curr;
				additional_waitings(n, additional_waiting_multiples, curr, wait_times[route]);
				// cout << endl;
			}

			for (auto w_set_1 : wait_times[1]) {			// for each set of waiting time multiples (to be added) to vertices of first route
				for (auto w_set_2 : wait_times[2]) {			// ... end each set of waiting time multiples to be added to the second route
					if (vrp_sol == v) {
						for (int w : w_set_1)
							cout << w << ",";							
						cout << " - ";
						for (int w : w_set_2)
							cout << w << ",";							
						cout << endl;
					}

					elapsed_time = (double) (clock() - start_clock) / CLOCKS_PER_SEC;
					if (elapsed_time >= last_elapsed_time_print + 10) {
						cout << "\t" << setw(8) << setprecision(1) << elapsed_time << setw(15) << setprecision(4) << best_sol.getCost() << setw(15) << n_sol << endl;
						last_elapsed_time_print = elapsed_time;
					}
					if (elapsed_time >= time_limit) {
						cout << "Timeout! " << endl;
						cout << "# feasible solutions found: " << n_sol << endl << endl;
						cout << "Best solution found:" << endl << best_sol.toString() << endl;
						cout << "R_CAPA Exp. cost: \t" << best_sol.getCost() << endl;	
						best_sol.setRecourseStrategy(R_CAPA_PLUS);
						cout << "R_CAPA+ Exp. cost: \t" << best_sol.getCost() << endl;
						
						if (scale != 1) {
							VRP_instance & I480 = VRP_instance_file::readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs(travel_times_file, instance_file, vehicle_capacity, 1);
							Solution_SS_VRPTW_CR s480(I480, strategy);
							s480.addRoutes(number_vehicles);
							s480.generateInitialSolution(true);
							s480.copyFromSol_scaleWaitingTimes(best_sol);
							cout << "Under scale 1:" << endl;
							cout << s480.toString() << endl << endl;
							cout << "R_CAPA Exp. cost: \t" << s480.getCost() << endl;	
							s480.setRecourseStrategy(R_CAPA_PLUS);
							cout << "R_CAPA+ Exp. cost: \t" << s480.getCost() << endl;
						}

						return 0;
					}

					// Set the waiting times, wrt additional waiting time multiples, to create the final solution
					for (int pos=1; pos <= (int)w_set_1.size(); pos++) {
						int region = s.getVertexAtPosition(1, pos).getRegion();
						int waiting_time = initial_waiting_time + waiting_time_multiple * w_set_1[pos-1];
						if (vrp_sol == v)
							cout << "Set " << waiting_time << " at route 1, pos " << pos << " (region " << region << ")" << endl;
						s.mipsol_setTAU(region, initial_waiting_time, 1, 0.0);
						s.mipsol_setTAU(region, waiting_time, 1, 1.0);
					}
					for (int pos=1; pos <= (int)w_set_2.size(); pos++) {
						int region = s.getVertexAtPosition(2, pos).getRegion();
						int waiting_time = initial_waiting_time + waiting_time_multiple * w_set_2[pos-1];
						if (vrp_sol == v)
							cout << "Set " << waiting_time << " at route 2, pos " << pos << " (region " << region << ")" << endl;
						s.mipsol_setTAU(region, initial_waiting_time, 2, 0.0);
						s.mipsol_setTAU(region, waiting_time, 2, 1.0);
					}
					s.update_waitingTimes_from_mipsol();

					if (vrp_sol == v)
						cout << s.toString(true) << "cost: " << s.getCost() << endl << "Old computation: " << s.computeExpectedCost_R_CAPA_old() << "  " << s.computeExpectedCost_R_INFTY_old() << endl;
					_ASSERT_(s.isFeasible(),"");

					if (s.getCost() < best_sol.getCost()) {
						best_sol = s;
						elapsed_time = (double) (clock() - start_clock) / CLOCKS_PER_SEC;
						cout << "\t" << setw(8) << setprecision(1) << elapsed_time << setw(15) << setprecision(4) << best_sol.getCost() << setw(15) << n_sol << endl;
					}

					// Restore the initial waiting times
					for (int pos=1; pos <= (int)w_set_1.size(); pos++) {
						int region = s.getVertexAtPosition(1, pos).getRegion();
						int waiting_time = initial_waiting_time + waiting_time_multiple * w_set_1[pos-1];
						s.mipsol_setTAU(region, waiting_time, 1, 0.0);
						s.mipsol_setTAU(region, initial_waiting_time, 1, 1.0);
					}
					for (int pos=1; pos <= (int)w_set_2.size(); pos++) {
						int region = s.getVertexAtPosition(2, pos).getRegion();
						int waiting_time = initial_waiting_time + waiting_time_multiple * w_set_2[pos-1];
						s.mipsol_setTAU(region, waiting_time, 2, 0.0);
						s.mipsol_setTAU(region, initial_waiting_time, 2, 1.0);
					}


					n_sol++;
				}
			}      
			// cout << endl << endl << endl << endl << endl << endl << endl << endl << endl << endl << endl << endl << endl << endl << endl << endl; 
		} 
	}

	cout << "\t" << setw(8) << setprecision(1) << elapsed_time << setw(15) << setprecision(4) << best_sol.getCost() << setw(15) << n_sol << "      FINISHED!" << endl << endl;

	cout << "# feasible solutions: " << n_sol << endl << endl;
	cout << "Best solution found:" << endl << best_sol.toString() << endl;
	// cout << best_sol.toString(true) << endl << endl;
	cout << "R_CAPA Exp. cost: \t" << best_sol.getCost() << endl;	
	best_sol.setRecourseStrategy(R_CAPA_PLUS);
	cout << "R_CAPA+ Exp. cost: \t" << best_sol.getCost() << endl;

	if (scale != 1) {
		VRP_instance & I480 = VRP_instance_file::readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs(travel_times_file, instance_file, vehicle_capacity, 1);
		Solution_SS_VRPTW_CR s480(I480, strategy);
		s480.addRoutes(number_vehicles);
		s480.generateInitialSolution(true);
		s480.copyFromSol_scaleWaitingTimes(best_sol);
		cout << "Under scale 1:" << endl;
		cout << s480.toString() << endl << endl;
		cout << "R_CAPA Exp. cost: \t" << s480.getCost() << endl;	
		s480.setRecourseStrategy(R_CAPA_PLUS);
		cout << "R_CAPA+ Exp. cost: \t" << s480.getCost() << endl;
	}

	return 0;
}














