/*
Copyright 2017 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/




#include "gurobi_c++.h"
#include <iomanip>  // setw, setfill
#include <cstdlib>
#include <cmath>
#include <sstream>
#include <limits>   // numeric_limits
#include <vector>

#include "VRP_instance.h"
#include "VRP_solution.h"
#include "tools.h"

using namespace std;


bool VRP_request::operator<(const VRP_request& other) const {
	if (operator==(other)) return false;

	if (revealTime < other.revealTime)              // reveal time first
	return true;
	if (revealTime == other.revealTime) {
		if (l < other.l)                    // end of time window second)
			return true;
		if (l == other.l) {
			ASSERT(vertex->region != other.vertex->region, "no tie breaking !");
			return vertex->region < other.vertex->region;   // region number to break ties
		}
	}

	return false;
}

string itos(int i) {stringstream s; s << i; return s.str(); }




#define WAIT_TIME_MULTIPLE  2



Solution_SS_VRPTW_CR* best_sol;
Solution_SS_VRPTW_CR* current_sol;
VRP_instance *I;

// Subtour elimination callback.  Whenever a feasible solution is found,
// find the smallest subtour, and add a subtour elimination constraint
// if the tour doesn't visit every node.
class SubtourElim: public GRBCallback
{
	public:
	GRBVar*** vars_x;
	GRBVar*** vars_tau;
	GRBVar* var_theta;
	SubtourElim(GRBVar*** x_vars, GRBVar*** tau_vars, GRBVar* theta_var) {
		vars_x = x_vars;
		vars_tau = tau_vars;
		var_theta = theta_var;
	}
	protected:
	void callback() {
		try {
			if (where == GRB_CB_MIPSOL) {
				// Found an integer feasible solution - does it visit every node?

				int NVertices = I->getNumberVertices(VRP_vertex::WAITING);
				int NVeh = I->getNumberVehicles();
				int H = I->getHorizon();
				// cout << endl << "MIPSOL" << endl;
				// RETRIEVE vars_x
				for (int i = 0; i < NVertices+1; i++) 
					for (int j = 0; j < NVertices+1; j++)
						current_sol->mipsol_setX(i, j, getSolution(vars_x[i][j], NVeh+1));

				// RETRIEVE vars_tau
				for (int i = 0; i < NVertices+1; i++) 
					for (int l = 0; l < H+1; l++)
						current_sol->mipsol_setTAU(i, l, getSolution(vars_tau[i][l], NVeh+1));

				// UPDATE the SSVRPTW solution using sol
				current_sol->update_routes_from_mipsol();


				// cout << s.toStringVariables() << endl;

				double theta = getSolution(*var_theta); 

				// SECs
				const vector<vector<Solution_SS_VRPTW_CR::MIPSol_Arc>> & subtours = current_sol->mipsol_getSubtours();
				for (auto& subtour : subtours) {
					// Add subtour elimination constraint for each route p
					for (int p = 1; p < NVeh+1; p++) {
						GRBLinExpr expr = 0;
						for (auto& arc : subtour)
							expr += vars_x[arc.i][arc.j][p];
						addLazy(expr <= subtour.size()-1);
					}
				}

				if (subtours.size() == 0) {   // If the solution is TOP-feasible
					// cout << s.next.toString() << endl;

					// cout << s.toStringVariables() << endl;

					// cout << "theta = " << theta << endl;

					current_sol->update_waitingTimes_from_mipsol();

					double E = current_sol->getCost();

					// // // DEBUG PROBAS
					// // double expect = s.experimentalExpectedCost(100000);
					// // double diff = 1 - min(E / expect , expect / E);
					// // cout << "Theta = "<< theta <<"\t Expected # rejects: " << E; 
					// // // cout << s.toStringProbaTimes() << endl;
					// // cout << "  Average # rejects: " << expect << "   Relative difference: " 
					// //       << outputMisc::greenExpr(diff > 0.00001) /* 0.001% */
					// //       << outputMisc::blueExpr(diff > 0.0001) /* 0.01% */
					// //       << outputMisc::redExpr(diff > 0.001) /* 0.1% */ << diff * 100 << " \%" << outputMisc::resetColor() << endl;
					// // // cout << s.toStringProbaTimes() << endl;


					if (theta < E) {
						GRBLinExpr expr_x = 0, expr_tau = 0;

						const vector< Solution_SS_VRPTW_CR::MIPSol_Arc > & arcs = current_sol->mipsol_getArcs();
						for (auto& arc : arcs)
							expr_x += vars_x[arc.i][arc.j][arc.p];

						const vector< Solution_SS_VRPTW_CR::MIPSol_WaitTime > & taus = current_sol->mipsol_getWaitTimes();
						for (auto& tau : taus)
							expr_tau += vars_tau[tau.i][tau.l][tau.p];

						addLazy(*var_theta >= E * ((expr_x + expr_tau) - (arcs.size() + taus.size()) + 1) );

						// cout << s.next.toString() << endl;
						// cout << sol.toStringVariables() << endl;
						// cout << expr_x << endl << expr_tau << endl << endl;
						// cout << "--------------------------------------------------------------------------------------------" << endl;

						// addLazy( expr_x + expr_tau <= sum_x + sum_tau - 1 );
						// addLazy( *var_theta <= E );

						// ASSERT(E == sol.getCost(), "Argh");
						if (current_sol->getCost() < best_sol->getCost()) {
							*best_sol = *current_sol;
							// cout << "New incumbent: " << sol.getCost() << endl;
						}

						// // Generating GENERAL OPTIMALITY CUT
						// MIPSOL_SSVRPTW partial_sol;
						// double E_partial = s.computeExpectedCost_withOptimalityCuts(best_sol->getCost(), sol, &partial_sol);

						// if(E_partial >= best_sol->getCost() ) {
						//   GRBLinExpr expr_x = 0, expr_tau = 0;
						//   int sum_x =0, sum_tau=0;
						//   partial_sol.get_GRBLinExpr_val_to_1(&expr_x, &expr_tau, vars_x, vars_tau, &sum_x, &sum_tau, true);
						//   addLazy(*var_theta >= E_partial * ((expr_x + expr_tau) - (sum_x+sum_tau) + 1) );
						//   // cout << s.next.toString() << endl;
						//   // cout << s.toStringProbaTimes() << endl;
						//   // cout << sol.toStringVariables() << endl;
						//   // cout << "PARTIAL SOL :" << endl;
						//   // cout << partial_sol.toStringVariables() << endl;
						//   // cout << "Incumbent cost: " << best_sol->getCost() << "    Added general cut for cost " << E_partial << " :" << endl;
						//   // cout << expr_x << endl << expr_tau << endl << endl;
						//   // cout << "--------------------------------------------------------------------------------------------" << endl;
						// }

					}

					// // TWO-in-ONE specific and general cut computation
					// MIPSOL_SSVRPTW partial_sol;

					// double theta_max = max(best_sol->getCost(), theta);
					// int *last_wait_ro = new int [NVeh +1];
					// int *last_wait_w = new int [NVeh +1];
					// double E_partial = s.computeExpectedCost_withOptimalityCuts(theta_max, sol, &partial_sol, last_wait_ro, last_wait_w);

					// if (E_partial < best_sol->getCost()) {
					// 	*best_sol = sol;
					// 	best_sol->setCost(E_partial);
					// 	// cout << "New incumbent: " << E_partial << endl;
					// 	// addLazy( *var_theta <= E_partial );
					// }
					// bool completed = (E_partial < theta_max);

					// GRBLinExpr expr_x = 0, expr_tau = 0;
					// int sum_x =0, sum_tau=0;
					// partial_sol.get_GRBLinExpr_val_to_1(&expr_x, &expr_tau, vars_x, vars_tau, &sum_x, &sum_tau, true);
					// // if (completed) {
					// //   addLazy( expr_x + expr_tau <= sum_x + sum_tau - 1 );
					// //   addLazy( *var_theta <= E_partial );
					// // } else
					// addLazy(*var_theta >= E_partial * ((expr_x + expr_tau) - (sum_x+sum_tau) + 1) );

					// // cout << sol.toStringVariables() << endl;
					// // sol.get_GRBLinExpr_val_to_1(&expr_x, &expr_tau, vars_x, vars_tau, &sum_x, &sum_tau, true);
					// // cout << expr_x << endl << expr_tau << endl << endl;
					// // cout << "PARTIAL SOL :" << endl;
					// // cout << partial_sol.toStringVariables() << endl;
					// // cout << "completed=" << completed;
					// // cout << "     Incumbent cost: " << best_sol->getCost() << "     theta=" << theta << "    Added general cut for cost " << E_partial << " :" << endl;
					// // partial_sol.get_GRBLinExpr_val_to_1(&expr_x, &expr_tau, vars_x, vars_tau, &sum_x, &sum_tau, !completed);
					// // cout << expr_x << endl << expr_tau << endl << endl;
					// // cout << "--------------------------------------------------------------------------------------------" << endl;

					// delete last_wait_ro; delete last_wait_w;
				}

			}
		} catch (GRBException e) {
			cout << "Error number: " << e.getErrorCode() << endl;
			cout << e.getMessage() << endl;
		} catch (...) {
			cout << "Error during callback" << endl;
		}
	}
};

int main(int argc, char *argv[]) {
	if (argc < 2) {
		cout << "Usage: ssvrptw_gurobi instance_file" << endl;
		return 1;
	}
	char * instance_file = argv[1];

	srand(time(NULL));
	I = & VRP_instance_file::readInstanceFile_SSVRPTW_CR(instance_file, 1.0);

	cout << I->toString() << endl;

	current_sol = new Solution_SS_VRPTW_CR(*I, R_INFTY);
	best_sol = new Solution_SS_VRPTW_CR(*I, R_INFTY);

	GRBEnv *env = NULL;
	GRBVar ***vars_x = NULL;
	GRBVar **vars_y = NULL;
	GRBVar ***vars_tau = NULL;
	int i,j,l;
	int NVertices = I->getNumberVertices(VRP_vertex::WAITING);
	int NVeh = I->getNumberVehicles();
	int H = I->getHorizon();

	vars_x = new GRBVar**[NVertices+1];
	for (i = 0; i < NVertices+1; i++) {
		vars_x[i] = new GRBVar*[NVertices+1];
		for (j = 0; j < NVertices+1; j++)
			vars_x[i][j] = new GRBVar[NVeh+1];
	}

	vars_y = new GRBVar*[NVertices+1];
	for (i = 0; i < NVertices+1; i++)
		vars_y[i] = new GRBVar[NVeh+1];

	vars_tau = new GRBVar**[NVertices+1];
	for (i = 0; i < NVertices+1; i++) {
		vars_tau[i] = new GRBVar*[H+1];
		for (l = 0; l < H+1; l++)
			vars_tau[i][l] = new GRBVar[NVeh+1];
	}

	try {
		int j,p;

		env = new GRBEnv();
		GRBModel model = GRBModel(*env);

		// Must set LazyConstraints parameter when using lazy constraints
		model.getEnv().set(GRB_IntParam_LazyConstraints, 1);

		// Create binary decision variables 
		for (i = 0; i < NVertices+1; i++)   // x variables
			for (j = 0; j < NVertices+1; j++)
				for (p = 0; p < NVeh +1; p++) 
					vars_x[i][j][p] = model.addVar(0.0, 1.0, 0.0, GRB_BINARY, "x_"+itos(i)+"_"+itos(j)+"_"+itos(p));

		for (i = 1; i < NVertices+1; i++)     // y variables
			for (p = 0; p < NVeh +1; p++)
				vars_y[i][p] = model.addVar(0.0, 1.0, 0.0, GRB_BINARY, "y_"+itos(i)+"_"+itos(p));

		for (i = 0; i < NVertices+1; i++)     // tau variables
			for (l = 0; l < H+1; l++) 
				for (p = 0; p < NVeh+1; p++)
					vars_tau[i][l][p] = model.addVar(0.0, 1.0, 0.0, GRB_BINARY, "tau_"+itos(i)+"_"+itos(l)+"_"+itos(p));


		// Create continuous theta variable
		GRBVar var_theta = model.addVar(0.0, 100.0, 1.0, GRB_CONTINUOUS, "theta");

		// Integrate new variables
		model.update();

		// ****************** Constraints *******************

		// Flow conservation constraints for customer vertices
		for (i = 0; i < NVertices+1; i++) {
			for (p = 1; p < NVeh +1; p++) {
				GRBLinExpr expr = 0, expr_ = 0;
				for (j = 0; j < NVertices+1; j++) {
					if (j == i) continue;
					expr += vars_x[i][j][p];
					expr_ += vars_x[j][i][p];
				}
				model.addConstr(expr == vars_y[i][p], "flow_"+itos(i)+"_"+itos(j)+"_"+itos(p));
				model.addConstr(expr_ == vars_y[i][p], "flow_"+itos(j)+"_"+itos(i)+"_"+itos(p));
			}
		}  

		GRBLinExpr expr, expr_;

		// Depot vertex visited by at most K routes
		expr = 0;
		for (p = 0; p < NVeh +1; p++)
			expr += vars_y[0][p];
		model.addConstr(expr <= NVeh, "max_route_0");

		// Each customer vertex visited by at most one route
		for (i = 1; i < NVertices+1; i++) {
			GRBLinExpr expr = 0;
			for (p = 0; p < NVeh +1; p++)
				expr += vars_y[i][p];
			model.addConstr(expr <= 1, "max_route_"+itos(i));
		}		

		// // Out and In degree of the depot vertex 
		// for (p = 1; p < NVeh+1; p++) {
		// 	expr = 0, expr_ = 0;
		// 	for (j = 0; j < NVertices+1; j++) {
		// 		expr += vars_x[0][j][p];
		// 		expr_ += vars_x[j][0][p];
		// 	}
		// 	model.addConstr(expr == 1, "outdeg1_depot_"+itos(p));
		// 	model.addConstr(expr_ == 1, "indeg1_depot"+itos(p));
		// }
		// // model.addConstr(expr >= 2, "deg_min2_depot");

		// Forbid edge from node back to itself 
		for (i = 0; i < NVertices+1; i++)
			for (p = 0; p < NVeh +1; p++)
				vars_x[i][i][p].set(GRB_DoubleAttr_UB, 0);


		// route p=0 contains nothing
		for (i = 0; i < NVertices+1; i++)   
			for (j = 0; j < NVertices+1; j++)
				vars_x[i][j][0].set(GRB_DoubleAttr_UB, 0);
		for (i = 0; i < NVertices+1; i++)   
			for (l = 0; l < H+1; l++)
				vars_tau[i][l][0].set(GRB_DoubleAttr_UB, 0);



		// WAITING TIMES ------------

		// Only one waiting time selected by waiting vertex
		for (i = 1; i < NVertices+1; i++) {
			for (p = 0; p < NVeh +1; p++) {
				expr = 0;
				for (l = 0; l < H+1; l++) 
					expr += vars_tau[i][l][p];
				model.addConstr(expr == vars_y[i][p], "wait_time_"+itos(i)+"_"+itos(p));
			}
		}


		// Max duration of each route p
		for (p = 1; p < NVeh +1; p++) {
			expr = 0;
			for (i = 0; i < NVertices+1; i++) 
				for (j = 0; j < NVertices+1; j++) {
					if (i == j) continue;
					expr += I->travelTime(i, j) * vars_x[i][j][p];
				}

			for (i = 1; i < NVertices+1; i++)
				for (l = 0; l < H+1; l++) 
					expr += vars_tau[i][l][p] * l;

			model.addConstr(expr <= H, "max_duration_"+itos(p));
		}



		// SYMMETRY BREAKING --------------
		// lex ordering
		for (p = 1; p < NVeh+1 -1; p++) {
			expr = 0, expr_ = 0;
			for (i = 1; i < NVertices+1; i++) {   // i in W
				expr += pow(2.0, i) * vars_y[i][p];
				expr_ += pow(2.0, i) * vars_y[i][p+1];
				// expr += log(boost::math::prime(i)) * vars_y[i][p];
				// expr_ += log(boost::math::prime(i)) * vars_y[i][p+1];
			}
			model.addConstr(expr <= expr_, "symmetry_breaking_"+itos(p));
		}

		// Restrict waiting times to 0, 5, 10, etc.
		for (i = 1; i < NVertices+1; i++)
			for (l = 0; l < H+1; l++)
				if ( (l % WAIT_TIME_MULTIPLE) != 0 )
					for (int p=1; p<NVeh+1; p++)
						vars_tau[i][l][p].set(GRB_DoubleAttr_UB, 0);



		// Set callback function
		SubtourElim cb = SubtourElim(vars_x, vars_tau, &var_theta);
		model.setCallback(&cb);


		// Optimize model
		model.optimize();

		// Extract solution
		if (model.get(GRB_IntAttr_SolCount) > 0) {

			// RETRIEVE vars_x
			for (int i = 0; i < NVertices+1; i++) 
				for (int j = 0; j < NVertices+1; j++)
					current_sol->mipsol_setX(i, j, model.get(GRB_DoubleAttr_X, vars_x[i][j], NVeh+1));

			// RETRIEVE vars_tau
			for (int i = 0; i < NVertices+1; i++) 
				for (int l = 0; l < H+1; l++)
					current_sol->mipsol_setTAU(i, l, model.get(GRB_DoubleAttr_X, vars_tau[i][l], NVeh+1));


			// UPDATE the SSVRPTW solution using sol
			current_sol->update_from_mipsol();
			cout << "Selected solution: " << current_sol->getCost() << endl << current_sol->toString();

			double *theta = model.get(GRB_DoubleAttr_X, &var_theta, 1);
			cout << "theta = " << *theta << endl;

			cout << "Best solution recorded: " << best_sol->getCost() << endl << best_sol->toString();
		}

		

	} catch (GRBException e) {
		cout << "Error number: " << e.getErrorCode() << endl;
		cout << e.getMessage() << endl;
	} catch (...) {
		cout << "Error during optimization" << endl;
	}


	return 0;
}
